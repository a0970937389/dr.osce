<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = 'projects';

    protected $fillable = [
        'teacher_id',
        'class_id',
        'name',
        'img_url',
        'index_id'
    ];
}
