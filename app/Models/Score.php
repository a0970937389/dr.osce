<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //
    protected $table = 'score';

    protected $fillable = [
        'user_id', 'score', 'tmt', 'tmc',
        'tmh',
        'tmm',
        'sct',
        'scc',
        'sch',
        'scm',
        'answeruser',
        'answercase',
        'his22',
        'his41',
        'meduser',
        'bmeduser',
        'medanswer',
        'his11',
        'his12',
        'his13',
        'his21',
        'his31',
        'his32',
        'his42',
        'his43',
        'his51',
        'his52',
        'his53',
        'his61',
        'criticalf',
        'criticals',
        'roundf',
        'questionf',
        'rounds',
        'questions',
        'tmhf',
        'tmhs',
        'tmht',
        'blood',
        'dissc1',
        'dissc2',
        'dissc3',
        'dissc4',
        'dissc5',
        'dissc6',
    ];
}
