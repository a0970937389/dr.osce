<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //
    protected $table = 'classes_t';

    protected $fillable = [
        'teacher_id',
        'class_name'
    ];
}
