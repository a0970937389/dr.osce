<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $table = 'classes_s';

    protected $fillable = [
        'student_id',
        'class_id',
        'class_name'
    ];
}
