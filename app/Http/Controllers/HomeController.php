<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Score;
use Log;
use App\Models\Student;
use App\Models\Project;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $score = Score::where('user_id','=',$user->id)
                            ->orderBy('id', 'desc')
                            ->get();
        if(empty($score)){
            $score = 0;
            return view('index', ['user'=>$user], ['score'=>$score]);
        }
        else{
            foreach ($score as $s){
                $s->created_at = date("Y-m-d H:i",strtotime($s->created_at));
            }
            return view('index', ['user'=>$user], ['score'=>$score]);
        }
    }

    public function getscore(){
        $user = Auth::user();
        $score = Score::where('user_id','=',$user->id)
                            ->orderBy('id', 'desc')
                            ->get();
        if(empty($score)){
            $score = 0;
            return $score;
        }
        else{
            foreach ($score as $s){
                $s->created_at = date("Y-m-d H:i:s",strtotime($s->created_at));
            }
            return $score;
        }
    }

    public function createScore(Request $request){
        Log::info($request);
        $user = Auth::user();
        Score::create([
            'user_id' => $user->id,
            'score' => $request->score,
            'tmt' => $request->tmt,
            'tmc' => $request->tmc,
            'tmh' => $request->tmh,
            'tmm' => $request->tmm,
            'sct' => $request->sct,
            'scc' => $request->scc,
            'sch' => $request->sch,
            'scm' => $request->scm,
            'answeruser' => $request->answeruser,
            'answercase' => $request->answercase,
            'his22' => $request->his22,
            'his41' => $request->his41,
            'meduser' => $request->meduser,
            'bmeduser' => $request->bmeduser,
            'medanswer' => $request->medanswer,
            'his11' => $request->his11,
            'his12' => $request->his12,
            'his13' => $request->his13,
            'his21' => $request->his21,
            'his31' => $request->his31,
            'his32' => $request->his32,
            'his42' => $request->his42,
            'his43' => $request->his43,
            'his51' => $request->his51,
            'his52' => $request->his52,
            'his53' => $request->his53,
            'his61' => $request->his61,
            'criticalf' => $request->criticalf,
            'criticals' => $request->criticals,
            'roundf' => $request->roundf,
            'questionf' => $request->questionf,
            'rounds' => $request->rounds,
            'questions' => $request->questions,
            'tmhf' => $request->tmhf,
            'tmhs' => $request->tmhs,
            'tmht' => $request->tmht,
            'blood' => $request->blood,
            'dissc1' => $request->dissc1,
            'dissc2' => $request->dissc2,
            'dissc3' => $request->dissc3,
            'dissc4' => $request->dissc4,
            'dissc5' => $request->dissc5,
            'dissc6' => $request->dissc6,
        ]);
    }

    public function home(){
        $user = Auth::user();
        $classes = Student::where('student_id','=',$user->id)
                                ->get()
                                ->toArray();
        return view('user.index', ['user'=>$user], ['classes'=>$classes]);
    }

    public function simulation(){
        $user = Auth::user();
        return view('user.simulation', ['user'=>$user]);
    }

    public function get_class_data($request){
        $class_content = Project::where('class_id','=',$request)
                                ->get()
                                ->toArray();
        return ['class_content'=>$class_content];
    }

    public function get_class_student($request){
        $class_student = Student::where('class_id','=',$request)
                                ->get()
                                ->toArray();
        foreach ($class_student as &$cs){
            $sname = User::where('id','=',$cs['student_id'])
                                ->get()
                                ->toArray();
            $cs['student_info'] =  $sname;
        }
        return ['class_student'=>$class_student];                        
    }
}
