<?php

namespace App\Services;

use Route;

class UserTemplateService
{
    public function getSidebarMenu()
    {
        $sidebarMenu = [
            [
                'url' => '#',
                'name' => '執行課程',
                'icon' => 'fas fa-chalkboard-teacher',
                'navName' => 'article',
                'is_active' => false,
                'children' => null,
            ]
            
        ];
        $currentNavName = with(Route::current())->action['navName'] ?? '';
        foreach ($sidebarMenu as $lv1 => $i)
        {
            if (is_array($i['children']))
            {
                foreach ($i['children'] as $lv2 => $j)
                {
                    if (isset($j['navName']) && $j['navName'] === $currentNavName)
                    {
                        $sidebarMenu[$lv1]['children'][$lv2]['is_active'] 
                                = $sidebarMenu[$lv1]['is_active']
                                = true;
                        break;
                    }
                }
                
                if ($sidebarMenu[$lv1]['is_active'])
                    break;
            }
            else if (isset($i['nav']) && $i['navName'] === $currentNavName)
            {
                $sidebarMenu[$lv1]['is_active'] = true;
                break;
            }
        }
        
        return $sidebarMenu;
    }
}