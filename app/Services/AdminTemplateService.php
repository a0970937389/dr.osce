<?php

namespace App\Services;

use Route;

class AdminTemplateService
{
    public function getSidebarMenu()
    {
        $sidebarMenu = [
            [
                'url' => 'class',
                'name' => '班級管理',
                'icon' => 'fas fa-address-book',
                'navName' => 'users',
                'is_active' => false,
                'children' => null,
            ], 
            [
                'url' => 'case_edit',
                'name' => '臨床案例建置',
                'icon' => 'fas fa-file-medical',
                'navName' => 'users',
                'is_active' => false,
                'children' => null,
            ], 
            [
                'url' => '#',
                'name' => '執行臨床案例',
                'icon' => 'fas fa-newspaper',
                'navName' => 'article',
                'is_active' => false,
                'children' => null,
            ],
            [
                'url' => '#',
                'name' => '診斷報告書',
                'icon' => 'fas fa-chalkboard-teacher',
                'navName' => 'article',
                'is_active' => false,
                'children' => null,
            ],
            /*
            [
                'url' => '#',
                'name' => '排程紀錄',
                'icon' => 'fas fa-tags',
                'navName' => 'Record',
                'is_active' => false,
                'children' => [
                    [
                        'url' => '#',
                        'name' => '庫存自動匯入紀錄',
                        'is_active' => false,
                    ],
                    [
                        'url' => '#',
                        'name' => '訂單自動匯出紀錄',
                        'is_active' => false,
                    ],
                    [
                        'url' => '#',
                        'name' => '訂單狀態自動更新匯入紀錄',
                        'is_active' => false,
                    ],
                ],
            ],*/
            
        ];
        $currentNavName = with(Route::current())->action['navName'] ?? '';
        foreach ($sidebarMenu as $lv1 => $i)
        {
            if (is_array($i['children']))
            {
                foreach ($i['children'] as $lv2 => $j)
                {
                    if (isset($j['navName']) && $j['navName'] === $currentNavName)
                    {
                        $sidebarMenu[$lv1]['children'][$lv2]['is_active'] 
                                = $sidebarMenu[$lv1]['is_active']
                                = true;
                        break;
                    }
                }
                
                if ($sidebarMenu[$lv1]['is_active'])
                    break;
            }
            else if (isset($i['nav']) && $i['navName'] === $currentNavName)
            {
                $sidebarMenu[$lv1]['is_active'] = true;
                break;
            }
        }
        
        return $sidebarMenu;
    }
}