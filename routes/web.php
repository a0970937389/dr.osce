<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/guest', function(){
    return view('index_guest');
});

Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');*/
Route::post('/home/save', 'HomeController@createScore');
Route::get('/home/getscore', 'HomeController@getscore');
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/home', ['middleware' => 'admin', function () {
    //
}]);
Route::get('/teacher/class', 'TeacherController@get_classes');
Route::get('/teacher/case_edit', 'TeacherController@case_edit');
Route::get('/student', 'HomeController@home');
Route::get('/student/simulation', 'HomeController@simulation');
Route::get('/get_class_data/{id}', 'HomeController@get_class_data');
Route::get('/get_class_student/{id}', 'HomeController@get_class_student');
Route::get('/test', function(){
    return view('test');
});
