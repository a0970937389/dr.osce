//核心執行順序:	(函式前方有加 "#" 的，就是內部有setInterval)
//前置					constructor()-> #waitloading()->
//第一次初始化			updateOutput()->exclude_updateOutput()->get_Preload_list()->
//						triggerFull()->fullScreen()->
//						start()->hideBlock()->preload()->startDive()-> #runtimeFun()*動態呼叫dive需要的(async)函式，每個函式內部都有一個setInterval* -> #checkcomplete()->
//			-------------------------------------------分隔線----------------------------------------------------
//開始切dive(初始化)	update()->updateOutput()->exclude_updateOutput()->【 gonext()->remove_preload()-> #updateInput() 註:比"第一次初始化"多的部分 】->get_Preload_list()->
//						start()->hideBlock()->preload()->startDive()-> #runtimeFun()*動態呼叫dive需要的(async)函式，每個函式內部都有一個setInterval* -> #checkcomplete()->
//			-------------------------------------------分隔線----------------------------------------------------
//開始切dive(初始化)	update()->...重複...
class DrOSCE_Controller {
    constructor() {
		console.log("constructor")
        this.outputList = {}; //後端儲存資料用，使用時記得要打成，drOSCE.outputList
        this.diveLinker = new DiveLinker("dive" + getID());
        this.waitloading();
    }
    waitloading() {
		console.log("waitloading")
        let _controller = this;
        let linker = this.diveLinker;
        _controller.start = setInterval(() => {
			console.log("waitloading_setInterval")
            if (!linker.initial)
                return linker.getIOList();
            clearInterval(_controller.start);
            delete _controller.start;
            linker.enableBlock(false)
            this.updateOutput();
            this.get_Preload_list();
            window.addEventListener("click", triggerFull, false);
        }, 100);
    }
    updateOutput() {
		console.log("updateOutput")
        return new Promise((resolve, reject) => {
            let linker = this.diveLinker;
            let new_outputs = linker.getOutputList();
            let keep_outputs = this.outputList;
            for (const key in new_outputs) {
                if (new_outputs.hasOwnProperty(key)) {
                    const output = new_outputs[key];
                    if (keep_outputs[output.name]) {
                        keep_outputs[output.name]["value"] = output.value;
                    } else {
                        keep_outputs[output.name] = {
                            name: output.name,
                            value: output.value
                        };
                    }
                }
            }
            this.exclude_updateOutput(["preloadNum", "gonext"]);
            resolve();
        });
    }
    exclude_updateOutput(exclude_Output_Name) {
		console.log("exclude_updateOutput")
        let keep_outputs = this.outputList;
        for (const key in exclude_Output_Name) {
            const exclude_name = exclude_Output_Name[key];
            if (keep_outputs[exclude_name])
                delete keep_outputs[exclude_name];
        }
    }
    start() {
		console.log("start")
		this.hideBlock();
        preload(getID());
        this.startDive();
        this.runtimeFun();
        checkcomplete();
    }
    update() {
        (async() => {
			console.log("update")
            await this.updateOutput();
            await gonext(getID(), getgonext());
            await this.updateInput();
            await this.get_Preload_list();
            await this.start();
        })();
    }
    updateInput() {
		console.log("updateInput")
        return new Promise((resolve, reject) => {
            let _controller = this;
            let linker = this.diveLinker;
            _controller.start = setInterval(() => {
			console.log("updateInput_setInterval")
                if (!linker.initial)
                    return linker.getIOList();
                clearInterval(_controller.start);
                delete _controller.start;
                let inputs = linker.getInputList();
                let outputList = this.outputList;
                let inputArray = [];
                for (const key in inputs) {
                    if (inputs.hasOwnProperty(key)) {
                        const input = inputs[key];
                        if (!outputList[input.name])
                            continue
                        let cache_output = outputList[input.name];
                        let obj = {
                            id: input.id,
                            value: cache_output.value
                        };
                        inputArray.push(obj);
                    }
                }
                linker.setInput(inputArray);
                resolve();
            }, 100);
        });
    }
    get_Preload_list() {
		console.log("get_Preload_list")
        return new Promise((resolve, reject) => {
            let linker = this.diveLinker;
            let outputs = linker.getOutputList();
            let prelArray = [];
            if (!preload_list[getID()]) {
                for (const key in outputs) {
                    if (outputs.hasOwnProperty(key)) {
                        const output = outputs[key];
                        let isRepeat = false;
                        if (output.name != "preloadNum")
                            continue
                            for (const tempkey in prelArray) {
                                if (prelArray[tempkey] == output.value) {
                                    isRepeat = true;
                                    break;
                                }
                            }
                        if (!isRepeat) {
                            prelArray.push(output.value);
                        }
                    }
                }
                preload_list[getID()] = prelArray;
            }
            resolve();
        });
    }
    hideBlock() {
		console.log("hideBlock")
        let linker = this.diveLinker;
        linker.enableBlock(false);
    }
    startDive() {
		console.log("startDive")
        let linker = this.diveLinker;
        linker.start();
    }
    runtimeFun() {
        console.log("呼叫runtimeFun")
        if (special_FunList[getID()]) {
            let linker = this.diveLinker;
            this.diveProject = undefined;
            this.runtimeFun = setInterval(() => {
                if (this.diveProject == undefined) {
                    console.log("在拿getProject")
                    this.diveProject = linker.getProject();
                } else {
                    console.log("已經拿到getProject")
                    let FunList = special_FunList[getID()];
                    if (FunList.length > 0) {
                        for (const key in FunList) {
                            let FunName = FunList[key];
                            console.log("正在執行runtimeFun")
                            try {
                                this[FunName]();
                            } catch (error) {
                                console.log("error:", error);
                            }
                        }
                    }
                    clearInterval(this.runtimeFun);
                    delete this.runtimeFun;
					console.log("結束runtimeFun")
                }
            }, 100)
        }
    }
    /***************************************** 以下為special_function*****************************************/
    getVarIDbyName(varName, objName) { //使用時記得要打成，drOSCE.getVarIDbyName()。功能:若只輸入"varName"變數的名字，則回傳所有此名子的id陣列
        let linker = this.diveLinker;
        let project = this.diveProject;
        let itemList = project.items;
        if (objName) {
            for (let objKey in itemList) {
                if (itemList.hasOwnProperty(objKey)
                     && itemList[objKey].hasOwnProperty("itemName")
                     && itemList[objKey].itemName == objName
                     && itemList[objKey].hasOwnProperty("attributes")) {
                    for (let varKey in itemList[objKey].attributes) {
                        if (itemList[objKey].attributes.hasOwnProperty(varKey)
                             && itemList[objKey].attributes[varKey].hasOwnProperty("attributeName")
                             && itemList[objKey].attributes[varKey].attributeName == varName) {
                            return varKey;
                        }
                    }
                }
            }
        } else {
            let varIDList = [];
            for (let objKey in itemList) {
                if (itemList.hasOwnProperty(objKey)
                     && itemList[objKey].hasOwnProperty("attributes")) {
                    for (let varKey in itemList[objKey].attributes) {
                        if (itemList[objKey].attributes.hasOwnProperty(varKey)
                             && itemList[objKey].attributes[varKey].hasOwnProperty("attributeName")
                             && itemList[objKey].attributes[varKey].attributeName == varName) {
                            varIDList[varIDList.length] = varKey;
                        }
                    }
                }
            }
            return varIDList;
        }
    }
    getObjNamebyVarID(varID) { //使用時記得要打成，drOSCE.getObjNamebyVarID()。
        let linker = this.diveLinker;
        let project = this.diveProject;
        let itemList = project.items;
        for (let objKey in itemList) {
            if (itemList.hasOwnProperty(objKey)
                 && itemList[objKey].hasOwnProperty("attributes")) {
                for (let varKey in itemList[objKey].attributes) {
                    if (itemList[objKey].attributes.hasOwnProperty(varKey)
                         && varKey == varID
                         && itemList[objKey].hasOwnProperty("itemName")) {
                        return itemList[objKey].itemName;
                    }
                }
            }
        }
    }
    //dive id:9844
    async QATree() {
        let _controller = this;
        let linker = this.diveLinker;
        let startID = getID();
        OptCheIDList = drOSCE.getVarIDbyName("Inquiry_chosen") //dive端用 "chosen" 回報選項方塊是否被點擊
        linker.setInput(drOSCE.getVarIDbyName("Inquiry_length", "Diag_rect"), showNumList.length) //提供dive端必要資訊
            _controller.QATree = setInterval(() => {
            console.log("QATree")
            if (startID != getID()) {
                clearInterval(_controller.QATree);
                delete _controller.QATree;
            } else {
                OptCheIDList.forEach(function (id) { //14個方塊輪流檢查是否被點擊
                    if (linker.getAttr(id) == "1") { //點擊後，後端進行處理 & 回饋
                        linker.setInput(id, 0) // "chosen" 變數初始化
                        let Inquiry_optNum = drOSCE.getVarIDbyName("Inquiry_optNum", drOSCE.getObjNamebyVarID(id));
                        Inquiry_optNum = parseInt(linker.getAttr(Inquiry_optNum)); //檢視被點擊的方塊，其 "optNum" 為多少
                        let node = getNode(showNumList[Inquiry_optNum]); //檢視 "optNum" 是指向齒輪(showNumList)的哪個位置
																		//並取得相對應的 "節點" ，以利後續利用
                        OptionRecord.push(showNumList[Inquiry_optNum]); // "依序" 紀錄學習歷程(點擊了哪個節點)
                        //回傳答案
                        let ans = node.answer; //取的 此節點 的 "answer" 屬性
                        if (ans) { //有些問題是不用回答的，eg:你好我是某某醫生。 因此有些節點的 "answer" 屬性沒有實質內容
                            let str = processStr(ans)
                                if (str.length > 16) //字串太長，需進行 "換行"
                                    str = str.slice(0, 16) + "\\n" + str.slice(16);
                                linker.setInput(drOSCE.getVarIDbyName("Inquiry_response", "Balloon_box"), str)
                        } else {
                            linker.setInput(drOSCE.getVarIDbyName("Inquiry_response", "Balloon_box"), 0)
                        }
                        //新增節點
                        if (node.nodePoint) { 
                            let npList = node.nodePoint; //檢視 "將出現" 的選項(可能多個)
                            for (let i = npList.length - 1; i > -1; i--) { //一個一個處理 "將出現" 的選項
                                if (showNumList.includes(npList[i]) || OptionRecord.includes(npList[i])) //已經出現的選項，不再出現
                                    continue;
                                let npNode = getNode(npList[i]); 
                                let conPass = true;
                                if (npNode.condition) { //檢視 "將出現" 的選項，是否有達到 "出現條件"(可能多個)
                                    let conList = npNode.condition;
                                    for (let j = 0; j < conList.length; j++) { //個別檢查 "出現條件" 是否通過
                                        for (let k = 0; k < conList[j].length; k++) { //一個 "出現條件" 可能有許多 "子條件"，
																	//eg:"肚子餓"&"有錢"-->"吃飯"，吃飯( "出現條件" )需要肚子餓和有錢("子條件")
                                            if (!(OptionRecord.includes(conList[j][k]))) {
                                                conPass = false;
                                                break;
                                            }
                                            conPass = true;
                                        }
                                        if (conPass)
                                            break; //其中一個 "出現條件" 成立即可
                                    }
                                }
                                if (!conPass)
                                    continue;
                                if (npNode.condition)
                                    delete npNode.condition; //如果 "有condition屬性" & 其中一個"出現條件"通過後，即可刪除屬性
                                showNumList.splice(Inquiry_optNum + 1, 0, npList[i]); //新增 "將出現" 的選項
                            }
                        }
                        //刪除節點
                        if (node.disableList[0].length < 3) { //刪除整個大標或小標
                            let disStr = node.disableList[0];
                            let needDelList = [];
                            for (let i in showNumList) {
                                if (showNumList[i].indexOf(disStr) == 0)
                                    needDelList.push(i);
                            }
                            for (let i = needDelList.length - 1; i > -1; i--) {
                                showNumList.splice(needDelList[i], 1);
                            }
                        } else { //依照disableList刪除
                            let disStr = node.disableList;
                            disStr.forEach(function (e) {
                                if (showNumList.indexOf(e) != -1)
                                    showNumList.splice(showNumList.indexOf(e), 1);
                            })
                        }
                        updateSQList(); //更新選項的"中文內容" (已經經過processStr(str)處理的)
                        Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]; //更新dive端的 "文字內容"
                        linker.setInput(drOSCE.getVarIDbyName("Inquiry_length", "Diag_rect"), showNumList.length) //提供dive端必要資訊
                    }
                });
            }
        }, 500);
    }
    async updateCnt() {
        let _controller = this;
        let linker = this.diveLinker;
        let startID = getID();
        Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]; //初始齒輪。機制設定dive端的選項方塊，數目為14個，
        _controller.updateCnt = setInterval(() => {							  //而Inquiry_optNumList主要用來記錄dive端方塊的 "Inquiry_optNum"。
			console.log("updateCnt")
            if (startID != getID()) {
                clearInterval(_controller.updateCnt);
                delete _controller.updateCnt;
            } else {
                for (let i = 0; i < 14; i++) { // 依序檢查方塊的 "Inquiry_optNum"
                    let Inquiry_optNum = drOSCE.getVarIDbyName("Inquiry_optNum", "Diag" + i);
                    Inquiry_optNum = parseInt(linker.getAttr(Inquiry_optNum));
                    if (Inquiry_optNumList[i] == Inquiry_optNum) //如果dive端的Inquiry_optNum有變，再進行文字內容更換，減輕系統負擔
                        continue;
                    let str = showQueList[Inquiry_optNum]; //取得齒輪相對應的 "中文內容" (已經經過processStr(str)處理的)
                    let size = 0;
                    if ((str.length > 18) && (size = 1)) //檢查字串長度，而過長時，dive端要進行 "字體大小" 調整
                        str = str.slice(0, 26) + "\\n" + str.slice(26);
                    linker.setInput([{ //更新dive端資料
                                "id": drOSCE.getVarIDbyName("Inquiry_optCnt", "Diag" + i), //選項方塊的 "文字內容"
                                "value": str
                            }, {
                                "id": drOSCE.getVarIDbyName("Inquiry_optType", "Diag" + i), //通知dive端是否為 "標題"
                                "value": showNumList[Inquiry_optNum].length
                            }, {
                                "id": drOSCE.getVarIDbyName("Inquiry_optSize", "Diag" + i), //通知dive端是否需要調整"字體大小" 
                                "value": size
                            },
                        ]);
                    Inquiry_optNumList[i] = Inquiry_optNum; //更新後端 "Inquiry_optNum" 的紀錄
                }
            }
        }, 100);
    }
    //dive id:10146
    async curePreload() {
        let _controller = this;
        let linker = this.diveLinker; //控制linker的
        let project = this.diveProject; //拿到dive更多資料，比如說items，能知道dive有哪些物件，進而知道變數id等。
        let startID = getID(); //紀錄呼叫此函式的專案ID
        _controller.curePreload = setInterval(() => { //自行更改"Function_Name"。使專案進行中，函式持續執行
			console.log("curePreload_once")
            if (startID != getID()) { //專案結束會自動停止
                clearInterval(_controller.curePreload); //自行更改"Function_Name"
                delete _controller.curePreload; //自行更改"Function_Name"
            } else {
                if (scenarioNum == 0) {
                    linker.setInput([{
                                "id": drOSCE.getVarIDbyName("preloadNum", "手術"),
                                "value": 10468
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "熱敷"),
                                "value": 9589
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "普拿疼"),
                                "value": 10468
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "休息"),
                                "value": 9589
                            },
                        ]);
                } else {
                    linker.setInput([{
                                "id": drOSCE.getVarIDbyName("preloadNum", "手術"),
                                "value": 10468
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "熱敷"),
                                "value": 10468
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "普拿疼"),
                                "value": 10468
                            }, {
                                "id": drOSCE.getVarIDbyName("preloadNum", "休息"),
                                "value": 10468
                            },
                        ]);
                }
                clearInterval(_controller.curePreload);
                delete _controller.curePreload;
            }
        }, 100);
    }
    async getOption_id10146() {
        let _controller = this;
        let linker = this.diveLinker;
        let project = this.diveProject;
        let startID = getID();
        _controller.getOption_id10146 = setInterval(() => {
			console.log("getOption_id10146")
            if (startID != getID()) {
                clearInterval(_controller.getOption_id10146);
                delete _controller.getOption_id10146;
            } else {
                dxOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_dxOption", "Decision"));
                cureOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_cureOption", "Medicine"));
            }
        }, 500);
    }
    //dive id:9589
    async getOption_id9589() {
        let _controller = this;
        let linker = this.diveLinker;
        let project = this.diveProject;
        let startID = getID();
        _controller.getOption_id9589 = setInterval(() => {
			console.log("getOption_id9589")
            if (startID != getID()) {
                clearInterval(_controller.getOption_id9589);
                delete _controller.getOption_id9589;
            } else {
                revisitOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_revisitOption", "背景(不要刪)"));
            }
        }, 500);
    }
    //dive id:10074
    // async chang() {
    //     let _controller = this;
    //     let linker = this.diveLinker;
    //     let project = this.diveProject;
    //     let startID = getID();
    //     _controller.chang = setInterval(() => {
	// 		console.log("chang_once")
    //         if (startID != getID()) {
    //             clearInterval(_controller.chang);
    //             delete _controller.chang;
    //         } else {
    //             analysis(OptionRecord, scenarioNum, dxOption, cureOption, revistOption);
    //             clearInterval(_controller.chang);
    //             delete _controller.chang;
    //         }
    //     }, 100);
    // }
    /*dive id:9573，測試用請無視
    async OneMore() {
    console.log("呼叫OneMore")
    let _controller = this;
    let linker = this.diveLinker;
    let startID = getID();
    let showOneMoreId = drOSCE.getVarIDbyName("showOneMore");
    let writeOneMoreId = drOSCE.getVarIDbyName("writeOneMore");
    _controller.OneMore = setInterval(() => {
    if (startID != getID()) {
    console.log("終止中OneMore")
    clearInterval(_controller.OneMore);
    delete _controller.OneMore;
    console.log("已終止OneMore")
    } else {
    console.log("正在執行OneMore")
    let wOM_Value = linker.getAttr(writeOneMoreId[0]);
    linker.setInput(showOneMoreId[0], wOM_Value);
    }
    }, 100);
    }
    async showOneMore() {
    let _controller = this;
    let linker = this.diveLinker;
    let startID = getID();
    _controller.showOneMore = setInterval(() => {
    if (startID != getID()) {
    clearInterval(_controller.showOneMore);
    delete _controller.showOneMore;
    } else {
    //console.log("test for run two async", linker.getIOList())
    }
    }, 1000);
    }
     */
    /*請使用模板
    async Function_Name(){  //自行更改"Function_Name"，也記得在全域變數"special_FunList"相對應的ID後的陣列加入相同名稱喔 ("Function_Name")
    let _controller = this;
    let linker = this.diveLinker;  						//控制linker的
    let project = this.diveProject;						//拿到dive更多資料，比如說items，能知道dive有哪些物件，進而知道變數id等。
    let startID = getID();								//紀錄呼叫此函式的專案ID
    _controller.Function_Name = setInterval(() => { 	//自行更改"Function_Name"。使專案進行中，函式持續執行
    if(startID != getID()){  						//專案結束會自動停止
    clearInterval(_controller.Function_Name);	//自行更改"Function_Name"
    delete _controller.Function_Name;			//自行更改"Function_Name"
    }else{
    .
    .
    do something   								//自定義程式區
    .
    .
    }
    }, 100);
    }
     */
    /***************************************** 以上為special_function*****************************************/
}

/***************************************** 以下為全螢幕功能*****************************************/
function triggerFull() {
	console.log("triggerFull")
    fullScreen();
    drOSCE.start();
    document.querySelector(".loading").classList.add("dive-hide")
    window.removeEventListener("click", triggerFull, false)
}
function fullScreen() {
	console.log("fullScreen")
    let app = document.querySelector("#dive-app")
        if (document.fullscreenEnabled ||
            document.webkitFullscreenEnabled ||
            document.mozFullScreenEnabled ||
            document.msFullscreenEnabled) {
            // Do fullscreen
            if (app.requestFullscreen) {
                app.requestFullscreen();
            } else if (app.webkitRequestFullscreen) {
                this.app.webkitRequestFullscreen();
            } else if (app.mozRequestFullScreen) {
                app.mozRequestFullScreen();
            } else if (app.msRequestFullscreen) {
                app.msRequestFullscreen();
            }
        }
}
/***************************************** 以上為全螢幕功能*****************************************/

/***************************************** 以下為prload功能*****************************************/
const preload_list = {};
function getID() { //可以抓到現在執行的dive ID，使用時直接呼叫即可，getID()
    let id = document.getElementsByClassName("dive");
    id = id[0].name;
    id = id.slice(4);
    return id;
}
function getgonext() {
    let next = drOSCE.diveLinker.getOutputList();
    next = Object.values(next);
    for (let i = 0; i < next.length; i++) {
        if (next[i].name == "gonext") {
            next = Number(next[i].value);
            return next;
        }
    }
}
function preload(nowid) {
    //removeadd();
    if(preloaded.hasOwnProperty(getID()) == false){
        preloaded[getID()] = 0;
    }
    if(nowid == 10469){
		accin();
		checkleave();
    }
    if(nowid == 10468 && cansave == 1){
        analysis(OptionRecord, scenarioNum, dxOption, cureOption, revisitOption);
    }
	if (nowid in preload_list) {
        let pre = preload_list[nowid];
        for (let i = 0; i < pre.length; i++) {
            if(preloaded.hasOwnProperty(pre[i]) == false){
                preloaded[pre[i]] = 0;
                if(pre[i] == 10468){
                    var iframe = document.createElement('iframe');
                    iframe.className = "dive_hide";
                    iframe.name = "dive" + pre[i];
                    iframe.src = "http://dive.nutn.edu.tw:8080/Experiment//kaleTestExperiment5.jsp?eid=" + pre[i] + "&record=false";
                    document.getElementById("dive-app").appendChild(iframe);
                }
                else{
                    var iframe = document.createElement('iframe');
                    iframe.className = "dive_hide";
                    iframe.name = "dive" + pre[i];
                    iframe.src = "https://dive.nutn.edu.tw/Experiment//kaleTestExperiment5.jsp?eid=" + pre[i] + "&record=false";
                    document.getElementById("dive-app").appendChild(iframe);
                }
            }
            else{
                preloaded[pre[i]]++;
            }
        }
    }
    console.log(preloaded);
}
function checkleave(){
    checkleavetf = setInterval(function(){
        var complete = drOSCE.diveLinker.getOutputList();
        complete = Object.values(complete);
        for(let i=0; i<complete.length; i++){
            if(complete[i].name == "home_leave" && complete[i].value == 1){
                window.location.href = "/";
                clearInterval(checkleavetf);
            }
        }
    }, 100);
}
var remove = new Array();
function remove_preload() {
	$('.dive').css({"display": ""});
    $('.dive_hide').css({"display": "none"});
}
// function removeadd(){
//     remove = [];
//     let key, min = 99;
//     if(objsize(preloaded) > 10){
//         for(key in preloaded){
//             if(preloaded[key] < min){
//                 min = preloaded[key];
//             }
//         }
//     }
//     if(objsize(preloaded) > 10){
//         for(key in preloaded){
//             if(preloaded[key] == min){
//                 remove.push(key);
//             }
//         }
//         for(let i=0; i<=remove.length; i++){
//             $('iframe[name ="dive'+ remove[i] +'"]').remove();
//             delete preloaded[remove[i]];
//         }
//     }
// }
function objsize(obj){
    let size = 0, key;
    for(key in obj){
        if(obj.hasOwnProperty(key)) size++;
    }
    return size;
}
function checkcomplete() {
	console.log("checkcomplete")
    checkcompletetf = setInterval(function () {
		console.log("checkcomplete_setInterval")
        let complete = drOSCE.diveLinker.checkComplete();
        if (complete == true) {
            clearInterval(checkcompletetf);
            drOSCE.diveLinker.pause();
            drOSCE.update()
        }
    }, 500);
}
var cansave = 0;
var preloaded = new Object();
function gonext(nowid, nextid) {
    if(nowid == 10469){
        clearInterval(checkleavetf);
        clearInterval(sctime);
        var scin = getdata();
        console.log(scin);
    }
    cansave = 1;
	console.log("gonext")
    return new Promise((resolve, reject) => {
        if (preloaded.hasOwnProperty(nextid)) {
            preloaded[nowid]++;
            preloaded[nextid]++;
            let post = document.getElementsByName("dive" + nowid);
            let now = document.getElementsByName("dive" + nextid);
            post[0].classList.add("dive_hide");
            now[0].classList.add("dive");
            post[0].classList.remove("dive");
            now[0].classList.remove("dive_hide");
            post[0].src = post[0].src;
            drOSCE.diveLinker.changetTarget("dive" + nextid);
            remove_preload();
        }
        if(nowid == 10469 && nextid == 10468){
            inputdata(scin);
            cansave = 0;
        }
        resolve();
    });
}
/***************************************** 以上為prload功能*****************************************/

/***************************************** 以下為前後分離函式區*****************************************/

/* 椎間盤 20/08/31
var ProcessTree = { //流程樹
    "0": {
        course: "0",
        question: "基本資料",
        answer: "",
        disableList: [],
        "0": {
            course: "00",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "000",
                question: "你好，我是[使用者姓]醫師",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
                "0": {
                    course: "0000",
                    question: "請問是[隨機病人姓]小姐/先生嗎?",
                    answer: "是",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                    "0": {
                        course: "00000",
                        question: "請問您的工作是?",
                        answer: "[病人職業]",
                        disableList: ["0"],
                    },
                    "1": {
                        course: "00001",
                        question: "請問您是從事什麼職業?",
                        answer: "[病人職業]",
                        disableList: ["0"],
                    },
                },
                "1": {
                    course: "0001",
                    question: "請問是[隨機病人全名]小姐/先生嗎?",
                    answer: "是",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                },
                "2": {
                    course: "0002",
                    question: "請問你的大名是?今年幾歲?",
                    answer: "醫生好，我是[隨機病人全名]，[病人年齡(數字)]歲",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                },
            },
            "1": {
                course: "001",
                question: "你好，我是[使用者全名]醫生",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
            },
            "2": {
                course: "002",
                question: "你好，我是[使用者學校]的[使用者全名]",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
            },
        },
    },
    "1": {
        course: "1",
        question: "求診原因",
        answer: "",
        disableList: [],
        "0": {
            course: "10",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "100",
                question: "今天來是？",
                answer: "醫生我的[病發位置]會[症狀]",
                disableList: ["100", "101", "102", "103", "104"],
                nodePoint: ["1000", "1001", "1002", "1003", "1004"],
                "0": {
                    course: "1000",
                    question: "是[病發位置]的哪個位置?",
                    answer: "[疼痛位置1]",
                    disableList: ["1000"],
                    nodePoint: ["10000", "10001", "1000000"],
                    "0": {
                        course: "10000",
                        question: "除了[疼痛位置1]，[疼痛位置2]也很[症狀]?",
                        answer: "對，[疼痛位置2]也很[症狀]",
                        disableList: ["10000"],
                        nodePoint: ["100000", "1000000", "1000001", "1000002"],
                        "0": {
                            course: "100000",
                            question: "是[非病發位置2]的哪個位置?",
                            answer: "[非病發位置2]不會[症狀]",
                            disableList: ["100000"],
                            "0": {
                                course: "1000000",
                                question: "了解，你的[疼痛位置1][症狀]會傳導至[疼痛位置2] ",
                                answer: "",
                                disableList: ["1"],
                                condition: [["104", "1000"], ["10000"], ["10001"]],
                            },
                            "1": {
                                course: "1000001",
                                question: "了解，[疼痛位置1][症狀]傳導至[疼痛位置2][案例時間]了",
                                answer: "",
                                disableList: ["1"],
                                condition: [["1002", "10000"], ["1002", "10001"], ["104", "1002"]],
                            },
                            "2": {
                                course: "1000002",
                                question: "了解，[疼痛位置1][症狀]傳導至[非病發位置2][案例時間]了",
                                answer: "",
                                disableList: ["1"],
                                condition: [["1002", "10000"], ["1002", "10001"], ["104", "1002"]],
                            },
                        },
                    },
                    "1": {
                        course: "10001",
                        question: "只有[疼痛位置1]會[症狀]嗎?",
                        answer: "[疼痛位置2]也會[症狀]",
                        disableList: ["10001"],
                        nodePoint: ["100000", "1000000", "1000001", "1000002"],
                    },
                },
                "1": {
                    course: "1001",
                    question: "那你的[非病發位置1]會痛嗎?",
                    answer: "不會",
                    disableList: ["1001"],
                },
                "2": {
                    course: "1002",
                    question: "你痛多久了?",
                    answer: "[案例時間]",
                    disableList: ["1002"],
                    nodePoint: ["10020", "1000001", "1000002"],
                    "0": {
                        course: "10020",
                        question: "了解，[病發位置][症狀]且已經[案例時間]了",
                        answer: "",
                        disableList: ["1"],
                    },
                },
                "3": {
                    course: "1003",
                    question: "感覺很嚴重?",
                    answer: "要怎麼辦",
                    disableList: ["1003"],
                },
                "4": {
                    course: "1004",
                    question: "好我知道了",
                    answer: "",
                    disableList: ["1"],
                },
            },
            "1": {
                course: "101",
                question: "今天有沒有不舒服的地方？",
                answer: "有，我的[病發位置]會[症狀]",
                disableList: ["100", "101", "102", "103", "104"],
                nodePoint: ["1000", "1001", "1002", "1003", "1004"],
            },
            "2": {
                course: "102",
                question: "這次是哪裡不舒服？",
                answer: "醫生我的[病發位置]會[症狀]",
                disableList: ["100", "101", "102", "103", "104"],
                nodePoint: ["1000", "1001", "1002", "1003", "1004"],
            },
            "3": {
                course: "103",
                question: "這次看診是有什麼症狀嗎？",
                answer: "醫生我的[病發位置]會[症狀]",
                disableList: ["100", "101", "102", "103", "104"],
                nodePoint: ["1000", "1001", "1002", "1003", "1004"],
            },
            "4": {
                course: "104",
                question: "今天有哪些不舒服的地方？",
                answer: "醫生我的[病發位置]還有[疼痛位置2]會[症狀]",
                disableList: ["100", "101", "102", "103", "104"],
                nodePoint: ["1000", "1001", "1002", "1003", "1004", "1000000", "1000001", "1000002"],
            },
        },
    },
    "2": {
        course: "2",
        question: "疾病特徵",
        answer: "",
        disableList: [],
        "0": {
            course: "20",
            question: "症狀時間",
            answer: "",
            disableList: [],
            "0": {
                course: "200",
                question: "你最早發生這種症狀是什麼時候?",
                answer: "[案例復發時間]",
                disableList: ["200"],
                nodePoint: ["2000"],
                "0": {
                    course: "2000",
                    question: "多久會發生一次，一次會持續多久?",
                    answer: "[案例復發頻率]",
                    disableList: ["2000"],
                },
            },
            "1": {
                course: "201",
                question: "過去是否有相同症狀?",
                answer: "有",
                disableList: ["201"],
                nodePoint: ["2000", "2010"],
                "0": {
                    course: "2010",
                    question: "多久之前?",
                    answer: "[案例復發時間]",
                    disableList: ["2010"],
                },
            },
            "2": {
                course: "202",
                question: "這次是何時開始痛?",
                answer: "[案例時間]",
                disableList: ["202"],
            },
            "3": {
                course: "203",
                question: "這次症狀持續多久了?",
                answer: "到現在都還會[症狀]",
                disableList: ["203"],
            },
        },
        "1": {
            course: "21",
            question: "活動狀況",
            answer: "",
            disableList: [],
            "0": {
                course: "210",
                question: "是怎麼發生的呢?",
                answer: "[案例發生情境]",
                disableList: ["210"],
                nodePoint: ["2100"],
                "0": {
                    course: "2100",
                    question: "是什麼時候發生的?",
                    answer: "[案例時間]",
                    disableList: ["2100"],
                },
            },
            "1": {
                course: "211",
                question: "請問你是有做什麼運動或是從事什麼工作？",
                answer: "[案例發生情境]",
                disableList: ["211"],
                nodePoint: ["2100"],
            },
        },
        "2": {
            course: "22",
            question: "疼痛詢問",
            answer: "",
            disableList: [],
            "0": {
                course: "220",
                question: "請問你是哪裡[症狀]? ",
                answer: "[疼痛位置1]",
                disableList: ["220"],
                nodePoint: ["2200", "2201"],
                "0": {
                    course: "2200",
                    question: "請指出[症狀]的部位",
                    answer: "（指[疼痛位置1]）",
                    disableList: ["2200"],
                },
                "1": {
                    course: "2201",
                    question: "除了[疼痛位置1]還有哪些地方[症狀]?",
                    answer: "[疼痛位置2]",
                    disableList: ["2201"],
                },
            },
            "1": {
                course: "221",
                question: "如果0分不[症狀]、10分最[症狀]，由0到10分，您現在[症狀]的程度是幾分?",
                answer: "感覺蠻[症狀]的，[案例症狀程度]",
                disableList: ["221"],
            },
            "2": {
                course: "222",
                question: "可以描述一下[症狀]的特性嗎?",
                answer: "[案例症狀情境]",
                disableList: ["222"],
            },
            "3": {
                course: "223",
                question: "有沒有什麼情形會讓你更[症狀]?",
                answer: "長時間坐著或搬東西用力會更[症狀]",
                disableList: ["223"],
            },
            "4": {
                course: "224",
                question: "怎樣會比較不[症狀]?",
                answer: "[案例舒緩情境]",
                disableList: ["224"],
            },
        },
        "3": {
            course: "23",
            question: "改變與處理",
            answer: "",
            disableList: [],
            "0": {
                course: "230",
                question: "走路時會不會感覺怪怪的?",
                answer: "[疼痛位置2]會[症狀]",
                disableList: ["230"],
                nodePoint: ["2300"],
                "0": {
                    course: "2300",
                    question: "我等一下幫你做步態檢查?",
                    answer: "好",
                    disableList: ["2300"],
                },
            },
            "1": {
                course: "231",
                question: "你能走路嗎?",
                answer: "可以，但[疼痛位置2]會[症狀]",
                disableList: ["231"],
                nodePoint: ["2300"],
            },
            "2": {
                course: "232",
                question: "這期間有自行做過什麼處理或接受治療嗎?",
                answer: "[案例自行治療歷史]",
                disableList: ["232"],
                nodePoint: ["2320"],
                "0": {
                    course: "2320",
                    question: "是去哪裡治療?",
                    answer: "[案例自行治療行為1]與[案例自行治療行為2]",
                    disableList: ["2320"],
                },
            },
            "3": {
                course: "233",
                question: "症狀越來越佳或惡化？",
                answer: "[案例症狀發展]",
                disableList: ["233"],
            },
        },
    },
    "3": {
        course: "3",
        question: "相關病史",
        answer: "",
        disableList: [],
        "0": {
            course: "30",
            question: "藥物",
            answer: "",
            disableList: [],
            "0": {
                course: "300",
                question: "請問有對食物或藥物過敏嗎? ",
                answer: "沒有",
                disableList: ["300"],
                nodePoint: ["3000"],
                "0": {
                    course: "3000",
                    question: "好我知道了",
                    answer: "",
                    disableList: ["3"],
                },
            },
            "1": {
                course: "301",
                question: "平常有沒有服用什麼藥?",
                answer: "[藥物史1]",
                disableList: ["301"],
                nodePoint: ["3000", "3010"],
                "0": {
                    course: "3010",
                    question: "哪種藥，什麼時候吃?",
                    answer: "[藥物史2][藥物史3]",
                    disableList: ["3010"],
                    nodePoint: ["30100"],
                    "0": {
                        course: "30100",
                        question: "每天都會服用嗎?",
                        answer: "[藥物史4]",
                        disableList: ["30100"],
                    },
                },
            },
            "2": {
                course: "302",
                question: "您目前有服用任何藥物嗎?",
                answer: "[藥物史1]",
                disableList: ["302"],
                nodePoint: ["3000"],
            },
            "3": {
                course: "303",
                question: "您曾打過哪些疫苗呢?",
                answer: "最近沒有",
                disableList: ["303"],
                nodePoint: ["3000"],
            },
        },
        "1": {
            course: "31",
            question: "家族史",
            answer: "",
            disableList: [],
            "0": {
                course: "310",
                question: "您家族成員中有發生過相似的問題嗎?",
                answer: "沒有",
                disableList: ["310"],
                nodePoint: ["3000"],
            },
            "1": {
                course: "311",
                question: "有任何家族遺傳性疾病嗎?",
                answer: "沒有",
                disableList: ["311"],
                nodePoint: ["3000"],
            },
            "2": {
                course: "312",
                question: "家族中有人罹患糖尿病或心臟病嗎?",
                answer: "沒有",
                disableList: ["312"],
                nodePoint: ["3000"],
            },
        },
        "2": {
            course: "32",
            question: "旅遊史",
            answer: "",
            disableList: [],
            "0": {
                course: "320",
                question: "最近有出國或國內旅遊嗎?",
                answer: "沒有",
                disableList: ["320"],
                nodePoint: ["3000"],
            },
        },
        "3": {
            course: "33",
            question: "平時嗜好",
            answer: "",
            disableList: [],
            "0": {
                course: "330",
                question: "您抽菸嗎?一天抽多少?已抽多久了?",
                answer: "沒有",
                disableList: ["330"],
                nodePoint: ["3000"],
            },
            "1": {
                course: "331",
                question: "您喝酒嗎?您一週喝多少量?",
                answer: "沒有",
                disableList: ["331"],
                nodePoint: ["3000"],
            },
            "2": {
                course: "332",
                question: "平常會運動嗎?",
                answer: "很少",
                disableList: ["332"],
                nodePoint: ["3000"],
            },
        },
        "4": {
            course: "34",
            question: "其他疾病",
            answer: "",
            disableList: [],
            "0": {
                course: "340",
                question: "最近是否有受外傷，例如撞到頭、跌倒、閃到腰",
                answer: "[外傷史]",
                disableList: ["340"],
                nodePoint: ["3000"],
            },
            "1": {
                course: "341",
                question: "請問有沒有內科疾病，像是糖尿病、高血壓",
                answer: "沒有",
                disableList: ["341"],
                nodePoint: ["3000"],
            },
            "2": {
                course: "342",
                question: "以前有沒有動過手術嗎?",
                answer: "有，我以前動過盲腸炎手術",
                disableList: ["342"],
                nodePoint: ["3000", "3420", "3421", "3422"],
                "0": {
                    course: "3420",
                    question: "多久以前，手術情況如何?",
                    answer: "八年前，手術很成功",
                    disableList: ["3420"],
                    nodePoint: ["3000"],
                },
                "1": {
                    course: "3421",
                    question: "是什麼手術?",
                    answer: "盲腸炎手術",
                    disableList: ["3421"],
                    nodePoint: ["3000"],
                },
                "2": {
                    course: "3422",
                    question: "哪個醫生負責手術的?",
                    answer: "忘記了",
                    disableList: ["3422"],
                    nodePoint: ["3000"],
                },
            },
            "3": {
                course: "343",
                question: "您是否有心悸的感覺?",
                answer: "沒有",
                disableList: ["343"],
                nodePoint: ["3000"],
            },
            "4": {
                course: "344",
                question: "您是否感到生病了?",
                answer: "是",
                disableList: ["344"],
                nodePoint: ["3000"],
            },
            "5": {
                course: "345",
                question: "您有嘔吐嗎?",
                answer: "沒有",
                disableList: ["345"],
                nodePoint: ["3000"],
            },
        },
        "5": {
            course: "35",
            question: "身體改變",
            answer: "",
            disableList: [],
            "0": {
                course: "350",
                question: "是否改變排便習慣?",
                answer: "沒有",
                disableList: ["350"],
                nodePoint: ["3000"],
            },
            "1": {
                course: "351",
                question: "您是否注意過腫脹的腳踝?",
                answer: "沒有，腳踝好像沒有腫脹",
                disableList: ["351"],
                nodePoint: ["3000"],
            },
            "2": {
                course: "352",
                question: "您排尿時是否感到疼痛?",
                answer: "沒有",
                disableList: ["352"],
                nodePoint: ["3000"],
            },
            "3": {
                course: "353",
                question: "您的肌肉或關節是否疼痛?",
                answer: "腳好像有一點",
                disableList: ["353"],
                nodePoint: ["3000"],
            },
            "4": {
                course: "354",
                question: "您的手或腳是否有感到虛弱之處嗎?",
                answer: "左腳有沒力，會酸麻",
                disableList: ["354"],
                nodePoint: ["3000"],
            },
        },
    },
}
var scenario0 = { //情境 0
    "病發位置": "後背",
    "非病發位置1": "頭",
    "非病發位置2": "手",
    "症狀": "疼痛",
    "疼痛位置1": "下背部",
    "疼痛位置2": "左腳",
    "使用者姓": "歐",
    "使用者全名": "歐斯奇",
    "使用者學校": "育秀大學",
    "隨機病人姓": "沈",
    "隨機病人全名": "沈佳慧",
    "病人年齡(數字)": "36",
    "病人職業": "科技公司秘書",
    "案例時間": "三個月",
    "案例復發時間": "一年前",
    "案例復發頻率": "一年三次，大概兩三天",
    "案例發生情境": "之前去打保齡球之後就背痛",
    "案例自行治療歷史": "有去國術館推拿和復健科拉腰",
    "案例自行治療行為1": "自行吃止痛藥",
    "案例自行治療行為2": "去國術館推拿",
    "案例症狀程度": "約7分",
    "案例症狀情境": "我走路左腳很痛",
    "案例舒緩情境": "平躺讓雙腳彎曲時比較舒服",
    "案例症狀發展": "越來越嚴重",
    "案例自行治療的藥物情境": "去地區診所拿的，之前拖地時突然背痛會吃",
    "藥物史1": "止痛藥",
    "藥物史2": "去地區診所拿的",
    "藥物史3": " 之前拖地時突然背痛",
    "藥物史4": "後背痛才吃",
    "外傷史": "在家拖地突然背痛和之前去打保齡球投球時背痛且腰直不起來",
}
var scenario1 = { //情境 1
    "病發位置": "後背",
    "非病發位置1": "肩膀",
    "非病發位置2": "脖子",
    "症狀": "痠痛",
    "疼痛位置1": "尾椎",
    "疼痛位置2": "右腿",
    "使用者姓": "歐",
    "使用者全名": "歐斯奇",
    "使用者學校": "育秀大學",
    "隨機病人姓": "陳",
    "隨機病人全名": "陳孜昊",
    "病人年齡(數字)": "30",
    "病人職業": "Youtuber",
    "案例時間": "一個月",
    "案例復發時間": "一年前",
    "案例復發頻率": "一年三次，大概兩三天",
    "案例發生情境": "為了剪片常常坐著就是好幾個小時",
    "案例自行治療歷史": "復健科電療和熱敷",
    "案例自行治療行為1": "自行吃止痛藥",
    "案例自行治療行為2": "復健科電療和熱敷",
    "案例症狀程度": "約3分",
    "案例症狀情境": "我走路左腳有一點痛",
    "案例舒緩情境": "站著動動腰有比較好",
    "案例症狀發展": "時好時壞",
    "案例自行治療的藥物情境": "家裡的止痛藥",
    "藥物史1": "止痛藥",
    "藥物史2": "忘記了",
    "藥物史3": " 之前閃到，肌肉痠痛",
    "藥物史4": "痛才吃",
    "外傷史": "久坐到腳麻，麻完就一直背痛",
}
var scenarioNum = Math.floor(Math.random() * 2); //挑選劇情，0 or 1
var showNumList = ["0", "000", "001", "002", "1", "100", "101", "102", "103", "104",//目前dive可顯示的節點
    "2", "20", "200", "201", "202", "203", "21", "210", "211", "22",
    "220", "221", "222", "223", "224", "23", "230", "231", "232", "233",
    "3", "30", "300", "301", "302", "303", "31", "310", "311", "312",
    "32", "320", "33", "330", "331", "332", "34", "340", "341", "342",
    "343", "344", "345", "35", "350", "351", "352", "353", "354", ];
*/

//頸部腫 20/08/31
var ProcessTree = { //流程樹
    "0": {
        course: "0",
        question: "基本資料",
        answer: "",
        disableList: [],
        "0": {
            course: "00",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "000",
                question: "你好，我是[使用者姓]醫師",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
                "0": {
                    course: "0000",
                    question: "請問是[隨機病人姓]小姐/先生嗎?",
                    answer: "是",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                    "0": {
                        course: "00000",
                        question: "請問您的工作是?",
                        answer: "[病人職業]",
                        disableList: ["0"],
                    },
                    "1": {
                        course: "00001",
                        question: "請問您是從事什麼職業?",
                        answer: "[病人職業]",
                        disableList: ["0"],
                    },
                },
                "1": {
                    course: "0001",
                    question: "請問是[隨機病人全名]小姐/先生嗎?",
                    answer: "是",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                },
                "2": {
                    course: "0002",
                    question: "請問你的大名是?今年幾歲?",
                    answer: "醫生好，我是[隨機病人全名]，[病人年齡(數字)]歲",
                    disableList: ["0000", "0001", "0002"],
                    nodePoint: ["00000", "00001"],
                },
            },
            "1": {
                course: "001",
                question: "你好，我是[使用者全名]醫生",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
            },
            "2": {
                course: "002",
                question: "你好，我是[使用者學校]的[使用者全名]",
                answer: "",
                disableList: ["000", "001", "002"],
                nodePoint: ["0000", "0001", "0002"],
            },
        },
    },
    "1": {
        course: "1",
        question: "求診原因",
        answer: "",
        disableList: [],
        "0": {
            course: "10",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "100",
                question: "哪裡不舒服？",
                answer: "脖子有一顆東西",
                disableList: ["100"],
            },
            "1": {
                course: "101",
                question: "發現多久了？",
                answer: "大概兩三個禮拜",
                disableList: ["101"],
            },
            "2": {
                course: "102",
                question: "哪個位置？",
                answer: "後頸部上方",
                disableList: ["102"],
            },
        },
    },
    "2": {
        course: "2",
        question: "病史調查",
        answer: "",
        disableList: [],
        "0": {
            course: "20",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "200",
                question: "哪個位置？",
                answer: "後頸部上方",
                disableList: ["200"],
            },
            "1": {
                course: "201",
                question: "發現多久了？",
                answer: "大概兩三個禮拜",
                disableList: ["201"],
            },
        },
    },
    "3": {
        course: "3",
        question: "腫塊相關問題",
        answer: "",
        disableList: [],
        "0": {
            course: "30",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "300",
                question: "大小有變嗎？",
                answer: "沒有",
                disableList: ["300"],
            },
            "1": {
                course: "301",
                question: "軟硬度有變嗎？",
                answer: "沒有",
                disableList: ["301"],
            },
            "2": {
                course: "302",
                question: "會痛嗎?",
                answer: "不會",
                disableList: ["302"],
            },
            "3": {
                course: "303",
                question: "怎麼發現的?",
                answer: "按摩時摸到",
                disableList: ["303"],
            },
            "4": {
                course: "304",
                question: "以前有長過嗎？",
                answer: "沒有",
                disableList: ["304"],
            },
        },
    },
    "4": {
        course: "4",
        question: "全身性症狀",
        answer: "",
        disableList: [],
        "0": {
            course: "40",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "400",
                question: "那最近有沒有發燒? 燒很久嗎?",
                answer: "燒了3天 最高到38度",
                disableList: ["400"],
            },
            "1": {
                course: "401",
                question: "最近吃東西時，會吞嚥困難嗎?",
                answer: "吞嚥時會有點困難",
                disableList: ["401"],
            },
        },
    },
    "5": {
        course: "5",
        question: "耳朵症狀",
        answer: "",
        disableList: [],
        "0": {
            course: "50",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "500",
                question: "耳朵會不會聽不清楚或悶悶的？",
                answer: "會",
                disableList: ["500"],
                nodePoint: ["5001"],
                "1": {
                    course: "5001",
                    question: "哪一邊？",
                    answer: "兩邊",
                    disableList: ["5"],
                },
            },
        },
    },
    "6": {
        course: "6",
        question: "喉症狀",
        answer: "",
        disableList: [],
        "0": {
            course: "60",
            question: "",
            answer: "",
            disableList: [],
            "0": {
                course: "600",
                question: "痰會有血絲嗎？",
                answer: "偶爾有",
                disableList: ["600"],
            },
            "1": {
                course: "601",
                question: "吞嚥困難嗎？",
                answer: "沒有",
                disableList: ["601"],
            },
            "2": {
                course: "602",
                question: "容易嗆到嗎？",
                answer: "不會",
                disableList: ["602"],
            },
            "3": {
                course: "603",
                question: "喉嚨卡卡嗎？",
                answer: "會",
                disableList: ["603"],
            },
        },
    },
}
var scenario0 = { //情境 0
    "病發位置": "後背",
    "非病發位置1": "頭",
    "非病發位置2": "手",
    "症狀": "疼痛",
    "疼痛位置1": "下背部",
    "疼痛位置2": "左腳",
    "使用者姓": "歐",
    "使用者全名": "歐斯奇",
    "使用者學校": "歐奇大學",
    "隨機病人姓": "吳",
    "隨機病人全名": "吳家豪",
    "病人年齡(數字)": "36",
    "病人職業": "科技公司秘書",
    "案例時間": "三個月",
    "案例復發時間": "一年前",
    "案例復發頻率": "一年三次，大概兩三天",
    "案例發生情境": "之前去打保齡球之後就背痛",
    "案例自行治療歷史": "有去國術館推拿和復健科拉腰",
    "案例自行治療行為1": "自行吃止痛藥",
    "案例自行治療行為2": "去國術館推拿",
    "案例症狀程度": "約7分",
    "案例症狀情境": "我走路左腳很痛",
    "案例舒緩情境": "平躺讓雙腳彎曲時比較舒服",
    "案例症狀發展": "越來越嚴重",
    "案例自行治療的藥物情境": "去地區診所拿的，之前拖地時突然背痛會吃",
    "藥物史1": "止痛藥",
    "藥物史2": "去地區診所拿的",
    "藥物史3": " 之前拖地時突然背痛",
    "藥物史4": "後背痛才吃",
    "外傷史": "在家拖地突然背痛和之前去打保齡球投球時背痛且腰直不起來",
}
var scenarioNum = 0; //挑選劇情，0 or 1
var showNumList = ["0", "1", "3", "4", "5", "6", "2"]; //目前dive可顯示的節點

let register = { "0": ["000", "001", "002"], "1": ["100", "101", "102"], "2": ["200", "201"], "3": ["300", "301", "302", "303", "304"], "4": ["400", "401"], "5": ["500"], "6": ["600", "601", "602", "603"] };

var showQueList = []; //目前dive可顯示的節點，轉成中文
var OptionRecord = []; //學習歷程
var OptCheIDList = []; //dive"選項方塊"的ID
var Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]//初始齒輪
var dxOption;
var cureOption;
var revisitOption = "病人未回診";
function getNode(id) { //輸入節點id，會return節點(需拿變數接)
    let node = ProcessTree;
    for (let i in id) {
        node = node[id[i]];
    }
    return node;
}
function processStr(str) { //將"變數"換成相對應情境的"值"
    if (str) {
        let openBra = str.search("\\[");
        let closeBra = str.search("\\]");
        while (openBra != -1) {
            openBra = str.search("\\[");
            closeBra = str.search("\\]");
            let scenarioVar = str.substring(openBra + 1, closeBra);
            let scenarioValue = window["scenario" + scenarioNum][scenarioVar]; //動態變數的用法，抓相對應情境的內容
            scenarioVar = new RegExp("\\[" + scenarioVar + "\\]");
            str = str.replace(scenarioVar, scenarioValue);
        }
        return str;
    }
    return str;
}
function updateSQList() { //更新showQueList
    showQueList = [];
    for (let i in showNumList) {
        showQueList[i] = processStr(getNode(showNumList[i]).question);
    }
}
updateSQList();
/***************************************** 以上為前後分離函式區*****************************************/

/***************************************** 以下為診斷分析函式區*****************************************/
function analysis(or, sn, dx, cu, re) {
    let io = drOSCE.diveLinker.getInputList();
    io = Object.values(io);
    var count = 0;
    //var test=["00000","00001","00002","00003","0002","100","101","102","103","104","200","2010","203","221","223","224","301","332","420","210","211"]
    var tt = new Object();
    tt.score = {
        v: 0
    };
    tt.his11 = {
        s: ["00000", "00001", "00002"],
        v: 0
    };
    tt.his12 = {
        s: ["00000", "00001"],
        v: 0
    };
    tt.his13 = {
        s: "00002",
        v: 0
    };
    tt.his21 = {
        s: ["100", "101", "102", "103", ],
        v: 0
    };
    tt.his22 = {
        s: "104",
        v: 0
    };
    tt.his31 = {
        s: ["200", "2010"],
        v: 0
    };
    tt.his32 = {
        s: "203",
        v: 0
    };
    tt.his41 = {
        s: "221",
        v: 0
    };
    tt.his42 = {
        s: "223",
        v: 0
    };
    tt.his43 = {
        s: "224",
        v: 0
    };
    tt.his51 = {
        s: "301",
        v: 0
    };
    tt.his52 = {
        s: "332",
        v: 0
    };
    tt.his53 = {
        s: "420",
        v: 0
    };
    tt.his61 = {
        s: ["210", "211"],
        v: 0
    };
    tt.roundf = {
        s: "1",
        v: 0
    };
    tt.rounds = {
        s: "22",
        v: 0
    };
    tt.questionf = {
        s: "104",
        v: 0
    };
    tt.questions = {
        s: "224",
        v: 0
    };
    tt.criticalf = {
        s: "104",
        v: 0
    };
    tt.criticals = {
        s: "221",
        v: 0
    };
    tt.answeruser = {
        v: 0
    };
    tt.meduser = {
        v: 0
    };
    tt.bmeduser = {
        v: 0
    };
    tt.answercase = {
        v: 0
    };
    tt.medanswer = {
        v: 0
    };
    tt.tmt = {
        v: 0
    };
    tt.tmc = {
        v: 0
    };
    tt.tmh = {
        v: 0
    };
    tt.tmm = {
        v: 0
    };
    tt.sct = {
        v: 0
    };
    tt.scc = {
        v: 0
    };
    tt.sch = {
        v: 0
    };
    tt.scm = {
        v: 0
    };

    tt.tmhf = {
        v: 0
    };
    tt.tmhs = {
        v: 0
    };
    tt.tmht = {
        v: 0
    };
    tt.blood = {
        v: 0
    };
    tt.dissc1 = {
        v: 0
    };
    tt.dissc2 = {
        v: 0
    };
    tt.dissc3 = {
        v: 0
    };
    tt.dissc4 = {
        v: 0
    };
    tt.dissc5 = {
        v: 0
    };
    tt.dissc6 = {
        v: 0
    };

    for (let k = 0; k < or.length; k++) {
        if (or[k] == tt.his11.s[0] || or[k] == tt.his11.s[1] || or[k] == tt.his11.s[2]) {
            tt.his11.v = 1;
            count++;
            drOSCE.diveLinker.setInput("39d9250be89d4be8bdbb27d491e19246", tt.his11.v);
        }

        if (or[k] == tt.his12.s[0] || or[k] == tt.his12.s[1]) {
            tt.his12.v = 1;
            count++;
            drOSCE.diveLinker.setInput("4c6c01baa1834ecb91f4b954e138115c", tt.his12.v);
        }

        if (or[k] == tt.his13.s) {
            tt.his13.v = 1;
            count++;
            drOSCE.diveLinker.setInput("2ca823f44272426597c4cd29b981e3b0", tt.his13.v);
        }

        if (or[k] == tt.his21.s[0] || or[k] == tt.his21.s[1] || or[k] == tt.his21.s[2] || or[k] == tt.his21.s[3]) {
            tt.his21.v = 1;
            count++;
            drOSCE.diveLinker.setInput("e36de181ddae4f3ab75b5559cf75c426", tt.his21.v);
        }

        if (or[k] == tt.his22.s) {
            tt.his22.v = 1;
            count++;
            drOSCE.diveLinker.setInput("5838d981c7e842ba9f9d56fa9cf9233b", tt.his22.v);
        }

        if (or[k] == tt.his31.s[0] || or[k] == tt.his31.s[1]) {
            tt.his31.v = 1;
            count++;
            drOSCE.diveLinker.setInput("3dbd7bfbd91a4837a6b1215a6bce5d07", tt.his31.v);
        }

        if (or[k] == tt.his32.s) {
            tt.his32.v = 1;
            count++;
            drOSCE.diveLinker.setInput("af75e3b0c9114af6880c4b5689189d0c", tt.his32.v);
        }

        if (or[k] == tt.his41.s) {
            tt.his41.v = 1;
            count++;
            drOSCE.diveLinker.setInput("382a36a8051a4214926947ad80f68c33", tt.his41.v);
        }

        if (or[k] == tt.his42.s) {
            tt.his42.v = 1;
            count++;
            drOSCE.diveLinker.setInput("8aef53b4a8d14b139f4e648776081364", tt.his42.v);
        }

        if (or[k] == tt.his43.s) {
            tt.his43.v = 1;
            count++;
            drOSCE.diveLinker.setInput("f5ba3a23188e4ad4bbbb8815ec890dd8", tt.his43.v);
        }

        if (or[k] == tt.his51.s) {
            tt.his51.v = 1;
            count++;
            drOSCE.diveLinker.setInput("e0f842f6b1f943bf826a7cd042b36925", tt.his51.v);
        }

        if (or[k] == tt.his52.s) {
            tt.his52.v = 1;
            count++;
            drOSCE.diveLinker.setInput("22add313aa0d4bcd8a232c24e99fc79f", tt.his52.v);
        }

        if (or[k] == tt.his53.s) {
            tt.his53.v = 1;
            count++;
            drOSCE.diveLinker.setInput("2f72a4b565e14c1e82f571be427f5b1e", tt.his53.v);
        }

        if (or[k] == tt.his61.s[0] || or[k] == tt.his61.s[1]) {
            tt.his61.v = 1;
            count++;
            drOSCE.diveLinker.setInput("c138be12730f48438f986e61111b80ce", tt.his61.v);
        }
    }

    //answeruser

    if (dx == "軟組織受傷") {
        tt.answeruser.v = 1;
    }
    if (dx == "肌肉肌腱拉傷") {
        tt.answeruser.v = 2;
    }
    if (dx == "筋肌膜疼痛症候群") {
        tt.answeruser.v = 3;
    }
    if (dx == "椎間盤突出") {
        tt.answeruser.v = 4;
    }
    if (dx == "脊椎退化關節炎") {
        tt.answeruser.v = 5;
    }
    if (dx == "壓迫性骨折") {
        tt.answeruser.v = 6;
    }

    //meduser

    if (cu == "手術") {
        tt.meduser.v = 1;
    }
    if (cu == "熱敷") {
        tt.meduser.v = 2;
    }
    if (cu == "普拿疼") {
        tt.meduser.v = 3;
    }
    if (cu == "休息") {
        tt.meduser.v = 4;
    }

    //bmeduser

    if (re == "熱敷") {
        tt.bmeduser.v = 1;
    }
    if (re == "普拿疼") {
        tt.bmeduser.v = 2;
    }
    if (re == "手術") {
        tt.bmeduser.v = 3;
    }

    tt.answercase.v = sn;
    tt.sct.v = count * 5;
    tt.score.v = tt.sct.v + tt.scc.v + tt.sch.v + tt.scm.v;

    drOSCE.diveLinker.setInput("39540af51b6749cd929d0648e26e7008", tt.answeruser.v);

    drOSCE.diveLinker.setInput("beccd3ca72b140458793b2991ba72539", tt.meduser.v);

    drOSCE.diveLinker.setInput("9a79757fc4974cefa3a3900faed46008", tt.bmeduser.v);

    drOSCE.diveLinker.setInput("1f60b5073b5f4597895e1bf965f46ef3", tt.sct.diveProject);

    drOSCE.diveLinker.setInput("96aba748e9f341c983bbf9c1b04bb218", tt.answercase.v);

    drOSCE.diveLinker.setInput("c5c2d1f5930b4cdeb2aaa31f0f01a4eb", tt.score.v);

    //手術


    tt.tmh = {
        v: drOSCE.diveLinker.getAttr("b7c82d965bdb4b55807f174b24755ffd")
    };

    tt.blood = {
        v: drOSCE.diveLinker.getAttr("aeeaea7d7a194612b0b43daef9ce9bb3")
    };

    tt.dissc1 = {
        v: drOSCE.diveLinker.getAttr("412c8340baf146b68bd5c3d33248a778")
    };

    tt.dissc2 = {
        v: drOSCE.diveLinker.getAttr("7531fe50a7984a31a349bc302f7e2d47")
    };

    tt.dissc3 = {
        v: drOSCE.diveLinker.getAttr("a7febe07b34c466b967efeaa366342b1")
    };

    tt.dissc4 = {
        v: drOSCE.diveLinker.getAttr("03382ddc238a4f388a3f4a4b11d6f53e")
    };

    tt.dissc5 = {
        v: drOSCE.diveLinker.getAttr("c7697b66d07940aabb0e69820e66c49c")
    };

    tt.dissc6 = {
        v: drOSCE.diveLinker.getAttr("6486625686b94b60815ba9b0d704dd3b")
    };

    tt.sch.v = tt.tmh.v + tt.blood.v - 100;

    drOSCE.diveLinker.setInput("b6e6710ae52241f5b0809ba53bcdf3e5", tt.sch.v);
    savedata(tt);

}
function inputdata(datatt) {

    drOSCE.diveLinker.setInput("39540af51b6749cd929d0648e26e7008", datatt.answeruser);

    drOSCE.diveLinker.setInput("beccd3ca72b140458793b2991ba72539", datatt.meduser);

    drOSCE.diveLinker.setInput("9a79757fc4974cefa3a3900faed46008", datatt.bmeduser);

    drOSCE.diveLinker.setInput("1f60b5073b5f4597895e1bf965f46ef3", datatt.sct);

    drOSCE.diveLinker.setInput("96aba748e9f341c983bbf9c1b04bb218", datatt.answercase);

    drOSCE.diveLinker.setInput("c5c2d1f5930b4cdeb2aaa31f0f01a4eb", datatt.score);

    drOSCE.diveLinker.setInput("39d9250be89d4be8bdbb27d491e19246", datatt.his11);

    drOSCE.diveLinker.setInput("4c6c01baa1834ecb91f4b954e138115c", datatt.his12);

    drOSCE.diveLinker.setInput("2ca823f44272426597c4cd29b981e3b0", datatt.his13);

    drOSCE.diveLinker.setInput("e36de181ddae4f3ab75b5559cf75c426", datatt.his21);

    drOSCE.diveLinker.setInput("5838d981c7e842ba9f9d56fa9cf9233b", datatt.his22);

    drOSCE.diveLinker.setInput("3dbd7bfbd91a4837a6b1215a6bce5d07", datatt.his31);

    drOSCE.diveLinker.setInput("af75e3b0c9114af6880c4b5689189d0c", datatt.his32);

    drOSCE.diveLinker.setInput("382a36a8051a4214926947ad80f68c33", datatt.his41);

    drOSCE.diveLinker.setInput("8aef53b4a8d14b139f4e648776081364", datatt.his42);

    drOSCE.diveLinker.setInput("f5ba3a23188e4ad4bbbb8815ec890dd8", datatt.his43);

    drOSCE.diveLinker.setInput("e0f842f6b1f943bf826a7cd042b36925", datatt.his51);

    drOSCE.diveLinker.setInput("22add313aa0d4bcd8a232c24e99fc79f", datatt.his52);

    drOSCE.diveLinker.setInput("2f72a4b565e14c1e82f571be427f5b1e", datatt.his53);

    drOSCE.diveLinker.setInput("c138be12730f48438f986e61111b80ce", datatt.his61);
    //手術

    drOSCE.diveLinker.setInput("b7c82d965bdb4b55807f174b24755ffd", datatt.tmh);

    drOSCE.diveLinker.setInput("aeeaea7d7a194612b0b43daef9ce9bb3", datatt.blood);

    drOSCE.diveLinker.setInput("412c8340baf146b68bd5c3d33248a778", datatt.dissc1);

    drOSCE.diveLinker.setInput("7531fe50a7984a31a349bc302f7e2d47", datatt.dissc2);

    drOSCE.diveLinker.setInput("a7febe07b34c466b967efeaa366342b1", datatt.dissc3);

    drOSCE.diveLinker.setInput("03382ddc238a4f388a3f4a4b11d6f53e", datatt.dissc4);

    drOSCE.diveLinker.setInput("c7697b66d07940aabb0e69820e66c49c", datatt.dissc5);

    drOSCE.diveLinker.setInput("6486625686b94b60815ba9b0d704dd3b", datatt.dissc6);

    drOSCE.diveLinker.setInput("b6e6710ae52241f5b0809ba53bcdf3e5", datatt.sch);
}
/***************************************** 以上為診斷分析函式區*****************************************/

/***********************Code start************************************/
const special_FunList = {
    9573: ["OneMore", "showOneMore"],
    10652: ["QATree", "updateCnt"],
    10146: ["curePreload", "getOption_id10146"],
    10468: [],
    9589: ["getOption_id9589"]
};
const drOSCE = new DrOSCE_Controller();