//核心執行順序:	(函式前方有加 "#" 的，就是內部有setInterval)
//前置					constructor()-> #waitloading()->
//第一次初始化			updateOutput()->exclude_updateOutput()->get_Preload_list()->
//						triggerFull()->fullScreen()->
//						start()->hideBlock()->preload()->startDive()-> #runtimeFun()*動態呼叫dive需要的(async)函式，每個函式內部都有一個setInterval* -> #checkcomplete()->
//			-------------------------------------------分隔線----------------------------------------------------
//開始切dive(初始化)	update()->updateOutput()->exclude_updateOutput()->【 gonext()->remove_preload()-> #updateInput() 註:比"第一次初始化"多的部分 】->get_Preload_list()->
//						start()->hideBlock()->preload()->startDive()-> #runtimeFun()*動態呼叫dive需要的(async)函式，每個函式內部都有一個setInterval* -> #checkcomplete()->
//			-------------------------------------------分隔線----------------------------------------------------
//開始切dive(初始化)	update()->...重複...
class DrOSCE_Controller {
    constructor() {
        //console.log("constructor")
        this.outputList = {}; //後端儲存資料用，使用時記得要打成，drOSCE.outputList
        this.diveLinker = new DiveLinker("dive" + getID());
        this.waitloading();
    }
    waitloading() {
        //console.log("waitloading")
        let _controller = this;
        let linker = this.diveLinker;
        _controller.start = setInterval(() => {
            //console.log("waitloading_setInterval")
            if (!linker.initial)
                return linker.getIOList();
            clearInterval(_controller.start);
            delete _controller.start;
            linker.enableBlock(false)
            this.updateOutput();
            this.get_Preload_list();
            window.addEventListener("touchstart", triggerFull, false);
            window.addEventListener("click", triggerFull, false);
        }, 100);
    }
    updateOutput() {
        //console.log("updateOutput")
        return new Promise((resolve, reject) => {
            let linker = this.diveLinker;
            let new_outputs = linker.getOutputList();
            let keep_outputs = this.outputList;
            for (const key in new_outputs) {
                if (new_outputs.hasOwnProperty(key)) {
                    const output = new_outputs[key];
                    if (keep_outputs[output.name]) {
                        keep_outputs[output.name]["value"] = output.value;
                    } else {
                        keep_outputs[output.name] = {
                            name: output.name,
                            value: output.value
                        };
                    }
                }
            }
            this.exclude_updateOutput(["preloadNum", "gonext"]);
            resolve();
        });
    }
    exclude_updateOutput(exclude_Output_Name) {
        //console.log("exclude_updateOutput")
        let keep_outputs = this.outputList;
        for (const key in exclude_Output_Name) {
            const exclude_name = exclude_Output_Name[key];
            if (keep_outputs[exclude_name])
                delete keep_outputs[exclude_name];
        }
    }
    start() {
        //console.log("start")
        this.hideBlock();
        preload(getID());
        this.startDive();
        this.runtimeFun();
        checkcomplete();
    }
    update() {
        (async () => {
            //console.log("update")
            await this.updateOutput();
            await gonext(getID(), getgonext());
            await this.updateInput();
            await this.get_Preload_list();
            await this.start();
        })();
    }
    updateInput() {
        //console.log("updateInput")
        return new Promise((resolve, reject) => {
            let _controller = this;
            let linker = this.diveLinker;
            _controller.start = setInterval(() => {
                //console.log("updateInput_setInterval")
                if (!linker.initial)
                    return linker.getIOList();
                clearInterval(_controller.start);
                delete _controller.start;
                let inputs = linker.getInputList();
                let outputList = this.outputList;
                let inputArray = [];
                for (const key in inputs) {
                    if (inputs.hasOwnProperty(key)) {
                        const input = inputs[key];
                        if (!outputList[input.name])
                            continue
                        let cache_output = outputList[input.name];
                        let obj = {
                            id: input.id,
                            value: cache_output.value
                        };
                        inputArray.push(obj);
                    }
                }
                linker.setInput(inputArray);
                resolve();
            }, 100);
        });
    }
    get_Preload_list() {
        //console.log("get_Preload_list")
        return new Promise((resolve, reject) => {
            let linker = this.diveLinker;
            let outputs = linker.getOutputList();
            let prelArray = [];
            if (!preload_list[getID()]) {
                for (const key in outputs) {
                    if (outputs.hasOwnProperty(key)) {
                        const output = outputs[key];
                        let isRepeat = false;
                        if (output.name != "preloadNum")
                            continue
                        for (const tempkey in prelArray) {
                            if (prelArray[tempkey] == output.value) {
                                isRepeat = true;
                                break;
                            }
                        }
                        if (!isRepeat) {
                            prelArray.push(output.value);
                        }
                    }
                }
                preload_list[getID()] = prelArray;
            }
            resolve();
        });
    }
    hideBlock() {
        //console.log("hideBlock")
        let linker = this.diveLinker;
        linker.enableBlock(false);
    }
    startDive() {
        //console.log("startDive")
        let linker = this.diveLinker;
        linker.start();
    }
    runtimeFun() {
        //console.log("呼叫runtimeFun")
        if (special_FunList[getID()]) {
            let linker = this.diveLinker;
            this.diveProject = undefined;
            this.runtimeFun = setInterval(() => {
                if (this.diveProject == undefined) {
                    //console.log("在拿getProject")
                    this.diveProject = linker.getProject();
                } else {
                    //console.log("已經拿到getProject")
                    let FunList = special_FunList[getID()];
                    if (FunList.length > 0) {
                        for (const key in FunList) {
                            let FunName = FunList[key];
                            //console.log("正在執行runtimeFun")
                            try {
                                this[FunName]();
                            } catch (error) {
                                //console.log("error:", error);
                            }
                        }
                    }
                    clearInterval(this.runtimeFun);
                    delete this.runtimeFun;
                    //console.log("結束runtimeFun")
                }
            }, 100)
        }
    }
    /***************************************** 以下為special_function*****************************************/
    getVarIDbyName(varName, objName) { //使用時記得要打成，drOSCE.getVarIDbyName()。功能:若只輸入"varName"變數的名字，則回傳所有此名子的id陣列
        let linker = this.diveLinker;
        let project = this.diveProject;
        let itemList = project.items;
        if (objName) {
            for (let objKey in itemList) {
                if (itemList.hasOwnProperty(objKey)
                    && itemList[objKey].hasOwnProperty("itemName")
                    && itemList[objKey].itemName == objName
                    && itemList[objKey].hasOwnProperty("attributes")) {
                    for (let varKey in itemList[objKey].attributes) {
                        if (itemList[objKey].attributes.hasOwnProperty(varKey)
                            && itemList[objKey].attributes[varKey].hasOwnProperty("attributeName")
                            && itemList[objKey].attributes[varKey].attributeName == varName) {
                            return varKey;
                        }
                    }
                }
            }
        } else {
            let varIDList = [];
            for (let objKey in itemList) {
                if (itemList.hasOwnProperty(objKey)
                    && itemList[objKey].hasOwnProperty("attributes")) {
                    for (let varKey in itemList[objKey].attributes) {
                        if (itemList[objKey].attributes.hasOwnProperty(varKey)
                            && itemList[objKey].attributes[varKey].hasOwnProperty("attributeName")
                            && itemList[objKey].attributes[varKey].attributeName == varName) {
                            varIDList[varIDList.length] = varKey;
                        }
                    }
                }
            }
            return varIDList;
        }
    }
    getObjNamebyVarID(varID) { //使用時記得要打成，drOSCE.getObjNamebyVarID()。
        let linker = this.diveLinker;
        let project = this.diveProject;
        let itemList = project.items;
        for (let objKey in itemList) {
            if (itemList.hasOwnProperty(objKey)
                && itemList[objKey].hasOwnProperty("attributes")) {
                for (let varKey in itemList[objKey].attributes) {
                    if (itemList[objKey].attributes.hasOwnProperty(varKey)
                        && varKey == varID
                        && itemList[objKey].hasOwnProperty("itemName")) {
                        return itemList[objKey].itemName;
                    }
                }
            }
        }
    }
    //dive id:9844  ， 20/12/03更: 目前這個函式只負責"回答"對應問題，並記錄歷程而已
    async QATree() {
        let _controller = this;
        let linker = this.diveLinker;
        let startID = getID();
        let btnAudio = document.getElementById("btnAudio");
        OptCheIDList = drOSCE.getVarIDbyName("Inquiry_chosen") //dive端用 "chosen" 回報選項方塊是否被點擊
        linker.setInput(drOSCE.getVarIDbyName("Inquiry_length", "Diag_rect"), 0) //提供dive端必要資訊
        _controller.QATree = setInterval(() => {
            if (startID != getID()) {
                clearInterval(_controller.QATree);
                delete _controller.QATree;
            } else {
                OptCheIDList.forEach(function (id) { //14個方塊輪流檢查是否被點擊
                    if (linker.getAttr(id) == "1") { //點擊後，後端進行處理 & 回饋
                        linker.setInput(id, 0) // "chosen" 變數初始化
                        btnAudio.play();
                        let Inquiry_optNum = drOSCE.getVarIDbyName("Inquiry_optNum", drOSCE.getObjNamebyVarID(id)); //檢視被點擊的方塊，其 "optNum" 為多少
                        Inquiry_optNum = parseInt(linker.getAttr(Inquiry_optNum));
                        // let node = getNode(showNumList[Inquiry_optNum]); //檢視 "optNum" 是指向齒輪(showNumList)的哪個位置
                        let node = ProcessTree[showNumList[Inquiry_optNum]]; //檢視 "optNum" 是指向齒輪(showNumList)的哪個位置
                        //並取得相對應的 "節點" ，以利後續利用
                        //OptionRecord.push(showNumList[Inquiry_optNum]); // "依序" 紀錄學習歷程(點擊了哪個節點)
                        node.type = 3; //改變選像樣式「已點擊」
                        //回傳答案
                        let ans = node.answer; //取的 此節點 的 "answer" 屬性
                        ans = repeatQues(showNumList[Inquiry_optNum]) + ans;
                        OptionRecord.push(showNumList[Inquiry_optNum]); // "依序" 紀錄學習歷程(點擊了哪個節點)
                        if (ans) { //有些問題是不用回答的，eg:你好我是某某醫生。 因此有些節點的 "answer" 屬性沒有實質內容
                            let str = ans;
                            if (str.length > 16) //字串太長，需進行 "換行"
                                str = str.slice(0, 16) + "\\n" + str.slice(16);
                            linker.setInput(drOSCE.getVarIDbyName("Inquiry_response", "Balloon_box"), str);
                        } else {
                            linker.setInput(drOSCE.getVarIDbyName("Inquiry_response", "Balloon_box"), 0);
                        }
                        //解鎖子問題
                        if (node.subNode) {
                            let subNode = node.subNode;
                            subNode.forEach(nodeNum => {
                                ProcessTree[nodeNum].show = 1;
                            })
                        }
                    }
                });
            }
        }, 300);
    }
    async updateCnt() {
        let _controller = this;
        let linker = this.diveLinker;
        let startID = getID();
        Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]; //初始齒輪。機制設定dive端的選項方塊，數目為14個，
        _controller.updateCnt = setInterval(() => {	//而Inquiry_optNumList主要用來記錄dive端方塊的 "Inquiry_optNum"。
            if (startID != getID()) {
                clearInterval(_controller.updateCnt);
                delete _controller.updateCnt;
            } else {
                for (let i = 0; i < 14; i++) { // 依序檢查方塊的 "Inquiry_optNum"
                    let Inquiry_optNum = drOSCE.getVarIDbyName("Inquiry_optNum", "Diag" + i);
                    Inquiry_optNum = parseInt(linker.getAttr(Inquiry_optNum));
                    if ((Inquiry_optNumList[i] == Inquiry_optNum) || (i >= showNumList.length)) //如果dive端的Inquiry_optNum有變，再進行文字內容更換，減輕系統負擔
                        continue;
                    let str = showQueList[Inquiry_optNum]; //取得齒輪相對應的 "中文內容" (已經經過processStr(str)處理的)
                    let size = 0;
                    try {
                        if ((str.length > 18) && (size = 1)) //檢查字串長度，而過長時，dive端要進行 "字體大小" 調整
                            str = str.slice(0, 26) + "\\n" + str.slice(26);
                    } catch {
                        console.log(showNumList, showQueList, Inquiry_optNum, str);
                    }
                    // let node = getNode(showNumList[Inquiry_optNum]); //檢視 "optNum" 是指向齒輪(showNumList)的哪個位置
                    let node = ProcessTree[showNumList[Inquiry_optNum]]; //檢視 "optNum" 是指向齒輪(showNumList)的哪個位置
                    linker.setInput([{ //更新dive端資料
                        "id": drOSCE.getVarIDbyName("Inquiry_optCnt", "Diag" + i), //選項方塊的 "文字內容"
                        "value": str
                    }, {
                        "id": drOSCE.getVarIDbyName("Inquiry_optType", "Diag" + i), //通知dive端是否為 "標題"
                        "value": node.type
                    }, {
                        "id": drOSCE.getVarIDbyName("Inquiry_optSize", "Diag" + i), //通知dive端是否需要調整"字體大小" 
                        "value": size
                    },
                    ]);
                    Inquiry_optNumList[i] = Inquiry_optNum; //更新後端 "Inquiry_optNum" 的紀錄
                }
            }
        }, 100);
    }
    //dive id:10146
    async curePreload() {
        let _controller = this;
        let linker = this.diveLinker; //控制linker的
        let project = this.diveProject; //拿到dive更多資料，比如說items，能知道dive有哪些物件，進而知道變數id等。
        let startID = getID(); //紀錄呼叫此函式的專案ID
        _controller.curePreload = setInterval(() => { //自行更改"Function_Name"。使專案進行中，函式持續執行
            //console.log("curePreload_once")
            if (startID != getID()) { //專案結束會自動停止
                clearInterval(_controller.curePreload); //自行更改"Function_Name"
                delete _controller.curePreload; //自行更改"Function_Name"
            } else {
                if (scenarioNum == 0) {
                    linker.setInput([{
                        "id": drOSCE.getVarIDbyName("preloadNum", "手術"),
                        "value": 10468
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "熱敷"),
                        "value": 9589
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "普拿疼"),
                        "value": 10468
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "休息"),
                        "value": 9589
                    },
                    ]);
                } else {
                    linker.setInput([{
                        "id": drOSCE.getVarIDbyName("preloadNum", "手術"),
                        "value": 10468
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "熱敷"),
                        "value": 10468
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "普拿疼"),
                        "value": 10468
                    }, {
                        "id": drOSCE.getVarIDbyName("preloadNum", "休息"),
                        "value": 10468
                    },
                    ]);
                }
                clearInterval(_controller.curePreload);
                delete _controller.curePreload;
            }
        }, 100);
    }
    async getOption_id10146() {
        let _controller = this;
        let linker = this.diveLinker;
        let project = this.diveProject;
        let startID = getID();
        _controller.getOption_id10146 = setInterval(() => {
            //console.log("getOption_id10146")
            if (startID != getID()) {
                clearInterval(_controller.getOption_id10146);
                delete _controller.getOption_id10146;
            } else {
                dxOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_dxOption", "Decision"));
                cureOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_cureOption", "Medicine"));
            }
        }, 500);
    }
    //dive id:9589
    async getOption_id9589() {
        let _controller = this;
        let linker = this.diveLinker;
        let project = this.diveProject;
        let startID = getID();
        _controller.getOption_id9589 = setInterval(() => {
            //console.log("getOption_id9589")
            if (startID != getID()) {
                clearInterval(_controller.getOption_id9589);
                delete _controller.getOption_id9589;
            } else {
                revisitOption = linker.getAttr(drOSCE.getVarIDbyName("HIVD_revisitOption", "背景(不要刪)"));
            }
        }, 500);
    }
    async chang() {
        let _controller = this;
        let linker = this.diveLinker;
        let project = this.diveProject;
        let startID = getID();
        _controller.chang = setInterval(() => {
            if (startID != getID()) {
                clearInterval(_controller.chang);
                delete _controller.chang;
            } else {
                ask_compare();

                clearInterval(_controller.chang);
                delete _controller.chang;
            }
        }, 100);
    }
    //dive id:10074
    // async chang() {
    //     let _controller = this;
    //     let linker = this.diveLinker;
    //     let project = this.diveProject;
    //     let startID = getID();
    //     _controller.chang = setInterval(() => {
    // 		//console.log("chang_once")
    //         if (startID != getID()) {
    //             clearInterval(_controller.chang);
    //             delete _controller.chang;
    //         } else {
    //             analysis(OptionRecord, scenarioNum, dxOption, cureOption, revistOption);
    //             clearInterval(_controller.chang);
    //             delete _controller.chang;
    //         }
    //     }, 100);
    // }
    /*dive id:9573，測試用請無視
    async OneMore() {
    //console.log("呼叫OneMore")
    let _controller = this;
    let linker = this.diveLinker;
    let startID = getID();
    let showOneMoreId = drOSCE.getVarIDbyName("showOneMore");
    let writeOneMoreId = drOSCE.getVarIDbyName("writeOneMore");
    _controller.OneMore = setInterval(() => {
    if (startID != getID()) {
    //console.log("終止中OneMore")
    clearInterval(_controller.OneMore);
    delete _controller.OneMore;
    //console.log("已終止OneMore")
    } else {
    //console.log("正在執行OneMore")
    let wOM_Value = linker.getAttr(writeOneMoreId[0]);
    linker.setInput(showOneMoreId[0], wOM_Value);
    }
    }, 100);
    }
    async showOneMore() {
    let _controller = this;
    let linker = this.diveLinker;
    let startID = getID();
    _controller.showOneMore = setInterval(() => {
    if (startID != getID()) {
    clearInterval(_controller.showOneMore);
    delete _controller.showOneMore;
    } else {
    ////console.log("test for run two async", linker.getIOList())
    }
    }, 1000);
    }
     */
    /*請使用模板
    async Function_Name(){  //自行更改"Function_Name"，也記得在全域變數"special_FunList"相對應的ID後的陣列加入相同名稱喔 ("Function_Name")
    let _controller = this;
    let linker = this.diveLinker;  						//控制linker的
    let project = this.diveProject;						//拿到dive更多資料，比如說items，能知道dive有哪些物件，進而知道變數id等。
    let startID = getID();								//紀錄呼叫此函式的專案ID
    _controller.Function_Name = setInterval(() => { 	//自行更改"Function_Name"。使專案進行中，函式持續執行
    if(startID != getID()){  						//專案結束會自動停止
    clearInterval(_controller.Function_Name);	//自行更改"Function_Name"
    delete _controller.Function_Name;			//自行更改"Function_Name"
    }else{
    .
    .
    do something   								//自定義程式區
    .
    .
    }
    }, 100);
    }
     */
    /***************************************** 以上為special_function*****************************************/
}

/***************************************** 以下為全螢幕功能*****************************************/
function triggerFull() {
    console.log("triggerFull")
    fullScreen();
    drOSCE.start();
    document.querySelector(".loading").classList.add("dive-hide");
    window.removeEventListener("touchstart", triggerFull, false);
    window.removeEventListener("click", triggerFull, false);
    checkfull();
}
function fullScreen() {
    console.log("fullScreen")
    let app = document.querySelector("#dive-app")
    if (document.fullscreenEnabled ||
        document.webkitFullscreenEnabled ||
        document.mozFullScreenEnabled ||
        document.msFullscreenEnabled) {
        // Do fullscreen
        if (app.requestFullscreen) {
            app.requestFullscreen();
        } else if (app.webkitRequestFullscreen) {
            app.webkitRequestFullscreen();
        } else if (app.mozRequestFullScreen) {
            app.mozRequestFullScreen();
        } else if (app.msRequestFullscreen) {
            app.msRequestFullscreen();
        }
    }
}
/***************************************** 以上為全螢幕功能*****************************************/

/***************************************** 以下為prload功能*****************************************/
const preload_list = {};
function getID() { //可以抓到現在執行的dive ID，使用時直接呼叫即可，getID()
    let id = document.getElementsByClassName("dive");
    id = id[0].name;
    id = id.slice(4);
    return id;
}
function getgonext() {
    let next = drOSCE.diveLinker.getOutputList();
    next = Object.values(next);
    for (let i = 0; i < next.length; i++) {
        if (next[i].name == "gonext") {
            next = Number(next[i].value);
            return next;
        }
    }
}
function preload(nowid) {
    //removeadd();
    if (preloaded.hasOwnProperty(getID()) == false) {
        preloaded[getID()] = 0;
    }
    if (nowid == 14759) {
        checkleave();
    }
    if (nowid == 10468 && cansave == 1) {
        analysis(OptionRecord, scenarioNum, dxOption, cureOption, revisitOption);
    }
    if (nowid == 13133) {
        accin();
    }
    if (nowid == 13745) {
        askinit();
    }
    if (nowid == 10652) {
        askinit();
    }
    if (nowid in preload_list) {
        let pre = preload_list[nowid];
        for (let i = 0; i < pre.length; i++) {
            if (preloaded.hasOwnProperty(pre[i]) == false) {
                preloaded[pre[i]] = 0;
                if (pre[i] == 10468) {
                    var iframe = document.createElement('iframe');
                    iframe.className = "dive_hide";
                    iframe.name = "dive" + pre[i];
                    iframe.src = "http://dive.nutn.edu.tw:8080/Experiment//kaleTestExperiment5.jsp?eid=" + pre[i] + "&record=false";
                    document.getElementById("dive-app").appendChild(iframe);
                }
                else {
                    var iframe = document.createElement('iframe');
                    iframe.className = "dive_hide";
                    iframe.name = "dive" + pre[i];
                    iframe.src = "https://dive.nutn.edu.tw/Experiment//kaleTestExperiment5.jsp?eid=" + pre[i] + "&record=false";
                    document.getElementById("dive-app").appendChild(iframe);
                }
            }
            else {
                preloaded[pre[i]]++;
            }
        }
    }
    //console.log(preloaded);
}
function checkleave() {
    checkleavetf = setInterval(function () {
        var complete = drOSCE.diveLinker.getOutputList();
        complete = Object.values(complete);
        for (let i = 0; i < complete.length; i++) {
            if (complete[i].name == "home_leave" && complete[i].value == 1) {
                window.location.href = "/";
                clearInterval(checkleavetf);
            }
        }
    }, 100);
}
var remove = new Array();
function remove_preload() {
    $('.dive').css({ "display": "" });
    $('.dive_hide').css({ "display": "none" });
}
function removeadd() {
    remove = [];
    let key, min = 99;
    if (objsize(preloaded) > 10) {
        for (key in preloaded) {
            if (preloaded[key] < min) {
                min = preloaded[key];
            }
        }
    }
    if (objsize(preloaded) > 10) {
        for (key in preloaded) {
            if (preloaded[key] == min) {
                remove.push(key);
            }
        }
        for (let i = 0; i <= remove.length; i++) {
            $('iframe[name ="dive' + remove[i] + '"]').remove();
            delete preloaded[remove[i]];
        }
    }
}
function objsize(obj) {
    let size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}
function checkcomplete() {
    //console.log("checkcomplete")
    checkcompletetf = setInterval(function () {
        //console.log("checkcomplete_setInterval")
        let complete = drOSCE.diveLinker.checkComplete();
        if (complete == true) {
            clearInterval(checkcompletetf);
            drOSCE.diveLinker.pause();
            drOSCE.update()
        }
    }, 500);
}
var cansave = 0;
var preloaded = new Object();
function gonext(nowid, nextid) {
    if (nowid == 10469) {
        clearInterval(checkleavetf);
        clearInterval(sctime);
        var scin = getdata();
        //console.log(scin);
    }
    if (nowid == 10652) {
        clearInterval(checkstattf);
    }
    cansave = 1;
    //console.log("gonext")
    return new Promise((resolve, reject) => {
        if (preloaded.hasOwnProperty(nextid)) {
            preloaded[nowid]++;
            preloaded[nextid]++;
            let post = document.getElementsByName("dive" + nowid);
            let now = document.getElementsByName("dive" + nextid);
            post[0].classList.add("dive_hide");
            now[0].classList.add("dive");
            post[0].classList.remove("dive");
            now[0].classList.remove("dive_hide");
            post[0].src = post[0].src;
            drOSCE.diveLinker.changetTarget("dive" + nextid);
            remove_preload();
        }
        if (nowid == 10469 && nextid == 10468) {
            inputdata(scin);
            cansave = 0;
        }
        resolve();
    });
}
/***************************************** 以上為prload功能*****************************************/

/***************************************** 以下為前後分離函式區*****************************************/
var ProcessTree = [ //流程樹
    {
        question: "哪裡不舒服？",
        answer: "脖子有一顆東西",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [1, 2, 3, 42, 43, 44, 45, 46],
    },
    {
        question: "腫塊在哪裡?",
        answer: "頸部右側邊上半部(舉起右手，放在右臉頰下方脖子處)",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "怎麼發現的?",
        answer: "按摩時摸到的",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "大概出現多久了?",
        answer: "大約差不多2~3個月了",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "腫塊相關問題",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "腫塊有變大嗎?",
        answer: "有",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [6, 49],
    },
    {
        question: "會變大的很快嗎?",
        answer: "不會，變大的不太明顯",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "以前有長過嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "腫塊會痛嗎?",
        answer: "不會",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "相關症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "最近有感冒嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "最近頭皮或臉部是否有傷口？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "耳朵症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "最近耳朵有什麼不舒服嗎?",
        answer: "有，最近蠻常耳鳴還有耳朵悶悶的，有時候會聽不清楚",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [14],
    },
    {
        question: "哪一邊？",
        answer: "右",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "鼻子症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "最近有流鼻血或鼻涕帶血絲嗎?",
        answer: "會",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [17, 54, 66, 67, 68],
    },
    {
        question: "頻率很高嗎?",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "有鼻塞嗎？",
        answer: "有時候",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "喉症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "最近有出現血絲的情況嗎?",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "吞嚥困難嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "容易嗆到嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "喉嚨卡卡嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "視覺症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "會視力模糊嗎？",
        answer: "不會",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "最近看東西會兩個影子嗎？",
        answer: "會",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [27, 59],
    },
    {
        question: "頻率很高嗎?",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "其他症狀",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "是否有心悸、體重減輕或增加、容易流汗、便秘或腹瀉？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "是否有倦怠、身體痠痛？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "病史詢問",
        answer: "",
        type: 1, //1:標題，2:內容
        show: 0,
    },
    {
        question: "有吃檳榔的習慣嗎？",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "有抽菸的習慣嗎?",
        answer: "有",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [34],
    },
    {
        question: "一天大概幾包? 抽幾年了?",
        answer: "一天大概1包，已經抽20年了",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "有喝酒的習慣嗎?",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "有糖尿病或高血壓的問題嗎?",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "有癌症家族病史嗎?",
        answer: "好像有鼻咽癌",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    //多加的(下38開始)
    {
        question: "身體怎麼了?",
        answer: "脖子有一顆東西",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [1, 2, 3, 42, 43, 44, 45, 46],
    },
    {
        question: "腫塊在哪一邊?",
        answer: "頸部右側邊上半部(舉起右手，放在右臉頰下方脖子處)",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "怎麼了?",
        answer: "脖子有長東西",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [1, 2, 3, 42, 43, 44, 45, 46],
    },
    {
        question: "今天要看什麼",
        answer: "脖子腫腫的",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [1, 2, 3, 42, 43, 44, 45, 46],
    },
    {
        question: "要看什麼問題",
        answer: "脖子有一顆東西",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [1, 2, 3, 42, 43, 44, 45, 46],
    },
    {
        question: "腫塊在什麼部位",
        answer: "頸部右側邊上半部(舉起右手，放在右臉頰下方脖子處)",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "腫塊在什麼地方",
        answer: "頸部右側邊上半部(舉起右手，放在右臉頰下方脖子處)",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "怎麼摸到的？",
        answer: "按摩時摸到的",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "什麼時候發現的",
        answer: "大約差不多2~3個月了",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "何時發現的",
        answer: "大約差不多2~3個月了",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    //下47開始
    {
        question: "大小有變化嗎",
        answer: "有",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [6, 49],
    },
    {
        question: "大小有變嗎",
        answer: "有",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [6, 49],
    },
    {
        question: "變大多快",
        answer: "變大的不太明顯",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "以前有腫過嗎",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "會痛嗎",
        answer: "不會",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "會不會痛",
        answer: "不會",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "流鼻血嗎",
        answer: "會",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [17, 54, 66, 67, 68],
    },
    {
        question: "常常嗎",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    //下55開始
    {
        question: "鼻子會不會塞",
        answer: "有時候",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "鼻子塞住嗎",
        answer: "有時候",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "痰有沒有血",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "看東西怪怪嗎",
        answer: "會",
        type: 2, //1:標題，2:內容
        show: 1,
        subNode: [27, 59],
    },
    {
        question: "很常嗎",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 0,
    },
    {
        question: "心悸",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "體種減輕",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "盜汗",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "腸胃不舒服",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    //下63開始
    {
        question: "有沒有菸檳史",
        answer: "有抽煙",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "有沒有菸酒檳榔",
        answer: "有抽煙",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    //0330新增(下65)
    {
        question: "最近有其他不舒服嗎",
        answer: "沒有",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "很常發生嗎",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "多常發生",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "多久發生一次",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "最近有痰出現血絲的情況嗎",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    {
        question: "最近有鼻水/鼻涕出現血絲的情況嗎",
        answer: "偶爾",
        type: 2, //1:標題，2:內容
        show: 1,
    },
    //多加的end
    {
        question: "(都沒有我想問的)",
        answer: "",
        type: 2, //1:標題，2:內容
        show: 1,
    }
]
var showNumList = [ProcessTree.length - 1]; //目前dive可顯示的節點
var showQueList = []; //目前dive可顯示的節點，轉成中文
var OptionRecord = []; //學習歷程
var OptCheIDList = []; //dive"選項方塊"的ID
var Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]//初始齒輪
function updateSQList() { //更新showQueList
    showQueList = [];
    for (let i in showNumList) {
        // showQueList[i] = processStr(getNode(showNumList[i]).question);
        showQueList[i] = ProcessTree[showNumList[i]].question;
    }
}
function greaterNPercent(nodes) {
    showNumList = nodes;
    showNumList.push(ProcessTree.length - 1);
    updateSQList(); //更新選項的"中文內容" (已經經過processStr(str)處理的)
    Inquiry_optNumList = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]; //更新dive端的 "文字內容"
    drOSCE.diveLinker.setInput(drOSCE.getVarIDbyName("Inquiry_length", "Diag_rect"), showNumList.length) //提供dive端必要資訊
}
updateSQList();
function repeatQues(checkNum) {
    let munmun = [
        "醫生你不是問過了嗎? 我剛說，",
        "我剛好像有說，",
        "(你問過了，病人有點質疑的回)",
        "怎麼又問一次了? "
    ]
    if (OptionRecord.indexOf(checkNum) === -1 || (checkNum === ProcessTree.length - 1))
        return "";
    return munmun[Math.floor(Math.random() * munmun.length)]
}
/***************************************** 以上為前後分離函式區*****************************************/

/***************************************** 以下為診斷分析函式區*****************************************/
var dxOption;
var cureOption;
var revisitOption = "病人未回診";
function analysis(or, sn, dx, cu, re) {
    let io = drOSCE.diveLinker.getInputList();
    io = Object.values(io);
    var count = 0;
    //var test=["00000","00001","00002","00003","0002","100","101","102","103","104","200","2010","203","221","223","224","301","332","420","210","211"]
    var tt = new Object();
    tt.score = {
        v: 0
    };
    tt.his11 = {
        s: ["00000", "00001", "00002"],
        v: 0
    };
    tt.his12 = {
        s: ["00000", "00001"],
        v: 0
    };
    tt.his13 = {
        s: "00002",
        v: 0
    };
    tt.his21 = {
        s: ["100", "101", "102", "103",],
        v: 0
    };
    tt.his22 = {
        s: "104",
        v: 0
    };
    tt.his31 = {
        s: ["200", "2010"],
        v: 0
    };
    tt.his32 = {
        s: "203",
        v: 0
    };
    tt.his41 = {
        s: "221",
        v: 0
    };
    tt.his42 = {
        s: "223",
        v: 0
    };
    tt.his43 = {
        s: "224",
        v: 0
    };
    tt.his51 = {
        s: "301",
        v: 0
    };
    tt.his52 = {
        s: "332",
        v: 0
    };
    tt.his53 = {
        s: "420",
        v: 0
    };
    tt.his61 = {
        s: ["210", "211"],
        v: 0
    };
    tt.roundf = {
        s: "1",
        v: 0
    };
    tt.rounds = {
        s: "22",
        v: 0
    };
    tt.questionf = {
        s: "104",
        v: 0
    };
    tt.questions = {
        s: "224",
        v: 0
    };
    tt.criticalf = {
        s: "104",
        v: 0
    };
    tt.criticals = {
        s: "221",
        v: 0
    };
    tt.answeruser = {
        v: 0
    };
    tt.meduser = {
        v: 0
    };
    tt.bmeduser = {
        v: 0
    };
    tt.answercase = {
        v: 0
    };
    tt.medanswer = {
        v: 0
    };
    tt.tmt = {
        v: 0
    };
    tt.tmc = {
        v: 0
    };
    tt.tmh = {
        v: 0
    };
    tt.tmm = {
        v: 0
    };
    tt.sct = {
        v: 0
    };
    tt.scc = {
        v: 0
    };
    tt.sch = {
        v: 0
    };
    tt.scm = {
        v: 0
    };

    tt.tmhf = {
        v: 0
    };
    tt.tmhs = {
        v: 0
    };
    tt.tmht = {
        v: 0
    };
    tt.blood = {
        v: 0
    };
    tt.dissc1 = {
        v: 0
    };
    tt.dissc2 = {
        v: 0
    };
    tt.dissc3 = {
        v: 0
    };
    tt.dissc4 = {
        v: 0
    };
    tt.dissc5 = {
        v: 0
    };
    tt.dissc6 = {
        v: 0
    };

    for (let k = 0; k < or.length; k++) {
        if (or[k] == tt.his11.s[0] || or[k] == tt.his11.s[1] || or[k] == tt.his11.s[2]) {
            tt.his11.v = 1;
            count++;
            drOSCE.diveLinker.setInput("39d9250be89d4be8bdbb27d491e19246", tt.his11.v);
        }

        if (or[k] == tt.his12.s[0] || or[k] == tt.his12.s[1]) {
            tt.his12.v = 1;
            count++;
            drOSCE.diveLinker.setInput("4c6c01baa1834ecb91f4b954e138115c", tt.his12.v);
        }

        if (or[k] == tt.his13.s) {
            tt.his13.v = 1;
            count++;
            drOSCE.diveLinker.setInput("2ca823f44272426597c4cd29b981e3b0", tt.his13.v);
        }

        if (or[k] == tt.his21.s[0] || or[k] == tt.his21.s[1] || or[k] == tt.his21.s[2] || or[k] == tt.his21.s[3]) {
            tt.his21.v = 1;
            count++;
            drOSCE.diveLinker.setInput("e36de181ddae4f3ab75b5559cf75c426", tt.his21.v);
        }

        if (or[k] == tt.his22.s) {
            tt.his22.v = 1;
            count++;
            drOSCE.diveLinker.setInput("5838d981c7e842ba9f9d56fa9cf9233b", tt.his22.v);
        }

        if (or[k] == tt.his31.s[0] || or[k] == tt.his31.s[1]) {
            tt.his31.v = 1;
            count++;
            drOSCE.diveLinker.setInput("3dbd7bfbd91a4837a6b1215a6bce5d07", tt.his31.v);
        }

        if (or[k] == tt.his32.s) {
            tt.his32.v = 1;
            count++;
            drOSCE.diveLinker.setInput("af75e3b0c9114af6880c4b5689189d0c", tt.his32.v);
        }

        if (or[k] == tt.his41.s) {
            tt.his41.v = 1;
            count++;
            drOSCE.diveLinker.setInput("382a36a8051a4214926947ad80f68c33", tt.his41.v);
        }

        if (or[k] == tt.his42.s) {
            tt.his42.v = 1;
            count++;
            drOSCE.diveLinker.setInput("8aef53b4a8d14b139f4e648776081364", tt.his42.v);
        }

        if (or[k] == tt.his43.s) {
            tt.his43.v = 1;
            count++;
            drOSCE.diveLinker.setInput("f5ba3a23188e4ad4bbbb8815ec890dd8", tt.his43.v);
        }

        if (or[k] == tt.his51.s) {
            tt.his51.v = 1;
            count++;
            drOSCE.diveLinker.setInput("e0f842f6b1f943bf826a7cd042b36925", tt.his51.v);
        }

        if (or[k] == tt.his52.s) {
            tt.his52.v = 1;
            count++;
            drOSCE.diveLinker.setInput("22add313aa0d4bcd8a232c24e99fc79f", tt.his52.v);
        }

        if (or[k] == tt.his53.s) {
            tt.his53.v = 1;
            count++;
            drOSCE.diveLinker.setInput("2f72a4b565e14c1e82f571be427f5b1e", tt.his53.v);
        }

        if (or[k] == tt.his61.s[0] || or[k] == tt.his61.s[1]) {
            tt.his61.v = 1;
            count++;
            drOSCE.diveLinker.setInput("c138be12730f48438f986e61111b80ce", tt.his61.v);
        }
    }

    //answeruser

    if (dx == "軟組織受傷") {
        tt.answeruser.v = 1;
    }
    if (dx == "肌肉肌腱拉傷") {
        tt.answeruser.v = 2;
    }
    if (dx == "筋肌膜疼痛症候群") {
        tt.answeruser.v = 3;
    }
    if (dx == "椎間盤突出") {
        tt.answeruser.v = 4;
    }
    if (dx == "脊椎退化關節炎") {
        tt.answeruser.v = 5;
    }
    if (dx == "壓迫性骨折") {
        tt.answeruser.v = 6;
    }

    //meduser

    if (cu == "手術") {
        tt.meduser.v = 1;
    }
    if (cu == "熱敷") {
        tt.meduser.v = 2;
    }
    if (cu == "普拿疼") {
        tt.meduser.v = 3;
    }
    if (cu == "休息") {
        tt.meduser.v = 4;
    }

    //bmeduser

    if (re == "熱敷") {
        tt.bmeduser.v = 1;
    }
    if (re == "普拿疼") {
        tt.bmeduser.v = 2;
    }
    if (re == "手術") {
        tt.bmeduser.v = 3;
    }

    tt.answercase.v = sn;
    tt.sct.v = count * 5;
    tt.score.v = tt.sct.v + tt.scc.v + tt.sch.v + tt.scm.v;

    drOSCE.diveLinker.setInput("39540af51b6749cd929d0648e26e7008", tt.answeruser.v);

    drOSCE.diveLinker.setInput("beccd3ca72b140458793b2991ba72539", tt.meduser.v);

    drOSCE.diveLinker.setInput("9a79757fc4974cefa3a3900faed46008", tt.bmeduser.v);

    drOSCE.diveLinker.setInput("1f60b5073b5f4597895e1bf965f46ef3", tt.sct.diveProject);

    drOSCE.diveLinker.setInput("96aba748e9f341c983bbf9c1b04bb218", tt.answercase.v);

    drOSCE.diveLinker.setInput("c5c2d1f5930b4cdeb2aaa31f0f01a4eb", tt.score.v);

    //手術


    tt.tmh = {
        v: drOSCE.diveLinker.getAttr("b7c82d965bdb4b55807f174b24755ffd")
    };

    tt.blood = {
        v: drOSCE.diveLinker.getAttr("aeeaea7d7a194612b0b43daef9ce9bb3")
    };

    tt.dissc1 = {
        v: drOSCE.diveLinker.getAttr("412c8340baf146b68bd5c3d33248a778")
    };

    tt.dissc2 = {
        v: drOSCE.diveLinker.getAttr("7531fe50a7984a31a349bc302f7e2d47")
    };

    tt.dissc3 = {
        v: drOSCE.diveLinker.getAttr("a7febe07b34c466b967efeaa366342b1")
    };

    tt.dissc4 = {
        v: drOSCE.diveLinker.getAttr("03382ddc238a4f388a3f4a4b11d6f53e")
    };

    tt.dissc5 = {
        v: drOSCE.diveLinker.getAttr("c7697b66d07940aabb0e69820e66c49c")
    };

    tt.dissc6 = {
        v: drOSCE.diveLinker.getAttr("6486625686b94b60815ba9b0d704dd3b")
    };

    tt.sch.v = tt.tmh.v + tt.blood.v - 100;

    drOSCE.diveLinker.setInput("b6e6710ae52241f5b0809ba53bcdf3e5", tt.sch.v);
    savedata(tt);

}
function inputdata(datatt) {

    drOSCE.diveLinker.setInput("39540af51b6749cd929d0648e26e7008", datatt.answeruser);

    drOSCE.diveLinker.setInput("beccd3ca72b140458793b2991ba72539", datatt.meduser);

    drOSCE.diveLinker.setInput("9a79757fc4974cefa3a3900faed46008", datatt.bmeduser);

    drOSCE.diveLinker.setInput("1f60b5073b5f4597895e1bf965f46ef3", datatt.sct);

    drOSCE.diveLinker.setInput("96aba748e9f341c983bbf9c1b04bb218", datatt.answercase);

    drOSCE.diveLinker.setInput("c5c2d1f5930b4cdeb2aaa31f0f01a4eb", datatt.score);

    drOSCE.diveLinker.setInput("39d9250be89d4be8bdbb27d491e19246", datatt.his11);

    drOSCE.diveLinker.setInput("4c6c01baa1834ecb91f4b954e138115c", datatt.his12);

    drOSCE.diveLinker.setInput("2ca823f44272426597c4cd29b981e3b0", datatt.his13);

    drOSCE.diveLinker.setInput("e36de181ddae4f3ab75b5559cf75c426", datatt.his21);

    drOSCE.diveLinker.setInput("5838d981c7e842ba9f9d56fa9cf9233b", datatt.his22);

    drOSCE.diveLinker.setInput("3dbd7bfbd91a4837a6b1215a6bce5d07", datatt.his31);

    drOSCE.diveLinker.setInput("af75e3b0c9114af6880c4b5689189d0c", datatt.his32);

    drOSCE.diveLinker.setInput("382a36a8051a4214926947ad80f68c33", datatt.his41);

    drOSCE.diveLinker.setInput("8aef53b4a8d14b139f4e648776081364", datatt.his42);

    drOSCE.diveLinker.setInput("f5ba3a23188e4ad4bbbb8815ec890dd8", datatt.his43);

    drOSCE.diveLinker.setInput("e0f842f6b1f943bf826a7cd042b36925", datatt.his51);

    drOSCE.diveLinker.setInput("22add313aa0d4bcd8a232c24e99fc79f", datatt.his52);

    drOSCE.diveLinker.setInput("2f72a4b565e14c1e82f571be427f5b1e", datatt.his53);

    drOSCE.diveLinker.setInput("c138be12730f48438f986e61111b80ce", datatt.his61);
    //手術

    drOSCE.diveLinker.setInput("b7c82d965bdb4b55807f174b24755ffd", datatt.tmh);

    drOSCE.diveLinker.setInput("aeeaea7d7a194612b0b43daef9ce9bb3", datatt.blood);

    drOSCE.diveLinker.setInput("412c8340baf146b68bd5c3d33248a778", datatt.dissc1);

    drOSCE.diveLinker.setInput("7531fe50a7984a31a349bc302f7e2d47", datatt.dissc2);

    drOSCE.diveLinker.setInput("a7febe07b34c466b967efeaa366342b1", datatt.dissc3);

    drOSCE.diveLinker.setInput("03382ddc238a4f388a3f4a4b11d6f53e", datatt.dissc4);

    drOSCE.diveLinker.setInput("c7697b66d07940aabb0e69820e66c49c", datatt.dissc5);

    drOSCE.diveLinker.setInput("6486625686b94b60815ba9b0d704dd3b", datatt.dissc6);

    drOSCE.diveLinker.setInput("b6e6710ae52241f5b0809ba53bcdf3e5", datatt.sch);
}
function ask_compare() {
    if (OptionRecord.includes("0")) {
        drOSCE.diveLinker.setInput("f5547cd6efe74d6694ea5e7e7ef8feab", 1);
    };
    //"哪裡不舒服？" 有按到，然後哩
    if (OptionRecord.includes("5")) {
        drOSCE.diveLinker.setInput("1cb357e8b7e14ba0974117fdf078bfa3", 1);
    };
    //"腫塊有變大嗎?" 有按到，然後哩
    if (OptionRecord.includes("8")) {
        drOSCE.diveLinker.setInput("d8b5699c220540baac4457839bbc1edf", 1);
    };
    //"軟硬度有變嗎？" 有按到，然後哩

    let wantCheck = ["1", "3", "8"];
    let selectArr = [];
    let orderArr = [];
    wantCheck.forEach(item => {
        let order = OptionRecord.indexOf(item);
        if (order != -1) {
            selectArr.push(item);
            orderArr.push(order);
        }
    })
    for (let i = 0, len = orderArr.length; i < len; i++) {
        for (let j = 0; j < len - 1 - i; j++) {
            if (orderArr[j] > orderArr[j + 1]) {
                [orderArr[j], orderArr[j + 1]] = [orderArr[j + 1], orderArr[j]];
                [selectArr[j], selectArr[j + 1]] = [selectArr[j + 1], selectArr[j]];
            }
        }
    }
    let QA_result = ["0"]; //按照點擊的順序排，-1 是"相關症狀&病史詢問"
    let other = false;
    for (let i = 1, len = orderArr.length; i < len; i++) {
        if ((orderArr[i] - 1 != orderArr[i - 1]) && !other) {
            QA_result.push("-1");
            other = true;
        }
        QA_result.push(selectArr[i]);
    }

    for (let j = 0; j < 4; j++) {
        if (QA_result[j] == undefined) {
            QA_result[j] = "-1";
        }
    }
    console.log(QA_result);
    drOSCE.diveLinker.setInput("182f2f66c4924e4692298a95bf4a0bdf", QA_result[0]);
    drOSCE.diveLinker.setInput("197693902d524d39a7cd5223119d56cb", QA_result[1]);
    drOSCE.diveLinker.setInput("3161ac8a61514277ba208af61a5de870", QA_result[2]);
    drOSCE.diveLinker.setInput("da62e825895a4048afd2afc85ad97eec", QA_result[3]);
}
/***************************************** 以上為診斷分析函式區*****************************************/

/***********************Code start************************************/
const special_FunList = {
    9573: ["OneMore", "showOneMore"],
    10652: ["QATree", "updateCnt"],
    10146: ["curePreload", "getOption_id10146"],
    10468: [],
    9589: ["getOption_id9589"],
    14759: ["chang"],
};
const drOSCE = new DrOSCE_Controller();
