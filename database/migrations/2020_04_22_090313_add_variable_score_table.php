<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVariableScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('score', function (Blueprint $table) {
            $table->integer('tmt')->after('score')->default(0);
            $table->integer('tmc')->after('tmt')->default(0);
            $table->integer('tmh')->after('tmc')->default(0);
            $table->integer('tmm')->after('tmh')->default(0);
            $table->integer('sct')->after('tmm')->default(0);
            $table->integer('scc')->after('sct')->default(0);
            $table->integer('sch')->after('scc')->default(0);
            $table->integer('scm')->after('sch')->default(0);
            $table->integer('answeruser')->after('scm')->default(0);
            $table->integer('answercase')->after('answeruser')->default(0);
            $table->integer('his22')->after('answercase')->default(0);
            $table->integer('his41')->after('his22')->default(0);
            $table->integer('meduser')->after('his41')->default(0);
            $table->integer('bmeduser')->after('meduser')->default(0);
            $table->integer('medanswer')->after('bmeduser')->default(0);
            $table->integer('his11')->after('medanswer')->default(0);
            $table->integer('his12')->after('his11')->default(0);
            $table->integer('his13')->after('his12')->default(0);
            $table->integer('his21')->after('his13')->default(0);
            $table->integer('his31')->after('his21')->default(0);
            $table->integer('his32')->after('his31')->default(0);
            $table->integer('his42')->after('his32')->default(0);
            $table->integer('his43')->after('his42')->default(0);
            $table->integer('his51')->after('his43')->default(0);
            $table->integer('his52')->after('his51')->default(0);
            $table->integer('his53')->after('his52')->default(0);
            $table->integer('his61')->after('his53')->default(0);
            $table->integer('criticalf')->after('his61')->default(0);
            $table->integer('criticals')->after('criticalf')->default(0);
            $table->integer('roundf')->after('criticals')->default(0);
            $table->integer('questionf')->after('roundf')->default(0);
            $table->integer('rounds')->after('questionf')->default(0);
            $table->integer('questions')->after('rounds')->default(0);
            $table->integer('tmhf')->after('questions')->default(0);
            $table->integer('tmhs')->after('tmhf')->default(0);
            $table->integer('tmht')->after('tmhs')->default(0);
            $table->integer('blood')->after('tmht')->default(0);
            $table->integer('dissc1')->after('blood')->default(0);
            $table->integer('dissc2')->after('dissc1')->default(0);
            $table->integer('dissc3')->after('dissc2')->default(0);
            $table->integer('dissc4')->after('dissc3')->default(0);
            $table->integer('dissc5')->after('dissc4')->default(0);
            $table->integer('dissc6')->after('dissc5')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('score', function (Blueprint $table) {
            //
        });
    }
}
