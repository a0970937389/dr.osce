<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('classes_s')->insert([
            'student_id'=>'6',
            'class_id' => '1',
            'class_name'=>'測試課程',
        ]);
    }
}
