@extends('layouts.admin')
@section('content')
<body>
    <div class="row">
        <div class="col" style="padding: 5px 15px">
            <select id="class_select" class="text-left" onchange="select_change(this.options[this.options.selectedIndex].value)">
                <option value="x">請選擇班級</option>
            </select>
        </div>
        <div class="col text-right">
            <button class="btn btn-sm btn-outline-primary" >新增班級</button>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body" id="class_content">
                班級內容
            </div>
        </div>
    </div>
    <div id="app"></div>
</body>
@endsection

@section('scripts')
<script>
    var user = '{{ $user->name }}';
    var classes_name = new Array();
    var classes_id = new Array();
    var select = document.getElementById("class_select");
    var datatest;
    @foreach($classes as $class)
        classes_name.push("{{ $class['class_name'] }}");
        classes_id.push("{{ $class['id'] }}");
    @endforeach
    for(let i = 0; i < classes_id.length; i++){
        var option = document.createElement("option");
        option.text = classes_name[i];
        option.value = classes_id[i];
        select.appendChild(option);
    }
    function select_change(s){
        if(s == 'x'){
            document.getElementById("class_content").innerText = "班級內容";
        }
        else{
            var cc = document.getElementById("class_content");
            cc.innerText = '課程ID: ' + s + ' ';
            $.ajax({
                type: 'GET',
                url: '../get_class_student/' + s,
                success: function (data) {
                    datatest = data;
                    console.log(data);
                    for(let i=0; i<data["class_student"].length; i++){
                        if(data["class_student"][i].student_info.length != 0){
                            var cc = document.getElementById("class_content");
                            cc.innerHTML += "<br>";
                            cc.innerHTML += '學生id:' + data["class_student"][i].student_id + '&emsp;';
                            cc.innerHTML += '學生名稱:' + data["class_student"][i].student_info[0].name;
                        }
                    }
                },
                error: function() { 
                    console.log('datafail');
                }
            });
        }
    }
</script>
@endsection