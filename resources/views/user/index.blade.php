@extends('layouts.user')
@section('header')
<style>
    .class-img{
        display:  block;
        text-align:  center;
        width: 100%;
    }
</style>
@endsection
@section('content')
<body>
    <div class="row">
        <div class="col" style="padding: 5px 15px">
            <select id="class_select" class="text-left" onchange="select_change(this.options[this.options.selectedIndex].value)">
                <option value="x">請選擇班級</option>
            </select>
        </div>
    </div>
    <div class="row" style="padding: 5px 15px">
        <div class="card" style="width: 100%">
            <div class="card-body" id="class_content">

                <div class="row" style="margin: 5px; display: none" id="choose_mode">
                    <div class="col col-lg-6" style="width: 50%; padding: 5px">
                        <img style="width: 100%" src="img/simulation.png" onclick="sim_click()">
                    </div>
                    <div class="col col-lg-6" style="width: 50%; padding: 5px">
                        <img style="width:100%" src="img/knowledge.png">
                    </div>
                </div>

                <div id="datatable" class="row" style="margin: 5px; display: none">
                    <table id="table" class="table table-bordered table-hover dataTable no-footer">
                        <tr>
                            <th id="classname" colspan="2"></th>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div id="app"></div>
</body>
@endsection

@section('scripts')
<script>
    var user = '{{ $user->name }}';
    var classes_name = new Array();
    var classes_id = new Array();
    var chosen_class;
    var select = document.getElementById("class_select");
    @foreach($classes as $class)
        classes_name.push("{{ $class['class_name'] }}");
        classes_id.push("{{ $class['class_id'] }}");
    @endforeach
    for(let i = 0; i < classes_id.length; i++){
        var option = document.createElement("option");
        option.text = classes_name[i];
        option.value = classes_id[i];
        select.appendChild(option);
    }
    function select_change(s){
        if(s == 'x'){
            document.getElementById("datatable").style.display = "none";
            document.getElementById("choose_mode").style.display = "none";
        }
        else{
            document.getElementById("datatable").style.display = "none";
            document.getElementById("choose_mode").style.display = "";
            chosen_class = classes_id.indexOf(s);
            chosen_class = classes_name[chosen_class];
            document.getElementById("classname").innerText = chosen_class;
            var $myTable = $('#table');
            var rowElements = [];
            var i;
            $.ajax({
                type: 'GET',
                url: 'get_class_data/' + s,
                success: function (data) {
                    console.log(s);
                    console.log(data);
                    for ( i = 0; i < data.class_content.length; ++i ) {
                        rowElements.push(
                            $('<tr  style="display:table;"></tr>').append(
                                $('<td width="10%"></td>').html('<img src="'+ data.class_content[i].img_url + '" class="class-img">'),
                                $('<td></td>').html(data.class_content[i].name + '<br><button class="btn btn-success" style="margin: 10px 0px" onclick="javascript:location.href=' + "'student/simulation'" + '">進入課程</button>')
                            )
                        );
                    }
                    $myTable.append(rowElements);
                },
                error: function() { 
                    console.log('datafail');
                }
            });
        }
    }
    function sim_click(){
        document.getElementById("choose_mode").style.display = "none";
        document.getElementById("datatable").style.display = "";
    }
</script>
@endsection