<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dr.OSCE</title>
    <link rel="icon" href="../img/favicon.ico" type="image/x-icon" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" type="text/css" href="../css/loading.css" />
    <audio id="btnAudio">
        <source src="../media/主要_按紐(調).mp3" type="audio/mpeg">
        未取得按紐音效
    </audio>
</head>

<body>
    <div id="dive-app" class="container">
        <div class="loading" title="點擊後(全螢幕)並開始"></div>
		<!--<iframe src="http://dive.nutn.edu.tw:8080/Experiment//kaleTestExperiment5.jsp?eid=10469&record=false" name="dive10469" 
            class="dive" frameborder="0"></iframe>-->
        <iframe src="http://dive.nutn.edu.tw:8080/Experiment//kaleTestExperiment5.jsp?eid=13133&record=false" name="dive13133" 
            class="dive" frameborder="0"></iframe>
        
        <div class="float-div">
            <button class="float-button" onclick="fullScreen()">全螢幕</button>
        </div>
    </div>
	<script src="http://dive.nutn.edu.tw:8080/Experiment/js/dive.linker.min.js"></script>
    <script src="../js/linker.js"></script>
    <script>
        var user_name = '{{ $user->name}}';
        var ask_text, ask_stat, ask_sim;
        function accin(){
            var input = drOSCE.diveLinker.getInputList();
            input = Object.values(input);
            for(let i=0; i<input.length; i++){
                if(input[i].name == "user_name"){
                    drOSCE.diveLinker.setInput(input[i].id, user_name);
                }
            }
        }
        function checkleave(){
            checkleavetf = setInterval(function(){
                var complete = drOSCE.diveLinker.getOutputList();
                complete = Object.values(complete);
                for(let i=0; i<complete.length; i++){
                    if(complete[i].name == "leave" && complete[i].value == 1){
                        window.location.href = "../student";
                        clearInterval(checkleavetf);
                    }
                }
            }, 100);
        }
        function checkfull(){
            checkfull = setInterval(function(){
                if( window.innerHeight == screen.height) {
                    $( ".float-button" ).css( "display", "none" );
                }else{
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                        $( ".float-button" ).css( "display", "inline-block" );
                    }else{
                        $( ".float-button" ).css( "display", "inline-block" );
                        $( ".float-button" ).css( "padding", "5px 10px" );
                    }
                }
            }, 100);
        }
        function askinit(){
            var ask = drOSCE.diveLinker.getOutputList();
            ask = Object.values(ask);
            var PTid = [];
            for(let i=0; i<ask.length; i++){
                if(ask[i].name == "文字"){
                    ask_text = ask[i].id;
                }
                if(ask[i].name == "狀態"){
                    ask_stat = ask[i].id;
                }
                if(ask[i].name == "相似度"){
                    ask_sim = ask[i].id;
                }
            }
            checkstattf = setInterval(function(){
                if(drOSCE.diveLinker.getAttr(ask_stat) == 1){
                    PTid = [];
                    drOSCE.diveLinker.setInput(ask_stat, 2);
                    var text = drOSCE.diveLinker.getAttr(ask_text);
                    for(var PT in ProcessTree){
                        if(ProcessTree[PT].show == 1){
                            var sim = similar(text_streamline(text), text_streamline(ProcessTree[PT].question));
                            if(sim >= 0.5){
                                //console.log("yes" + testPT[PT].course + sim);
                                PTid.push(PT);
                            }
                            else{
                                //console.log("no" + testPT[PT].course + sim);
                            }
                        }
                    }
                    if(PTid.length == 0){
                        drOSCE.diveLinker.setInput(ask_sim, 999);
                        drOSCE.diveLinker.setInput(ask_stat, 0);
                    }
                    else{
                        drOSCE.diveLinker.setInput(ask_stat, 3);
                        greaterNPercent(PTid);
                        //console.log(PTid);
                    }
                }
            }, 100);
        }
        function similar(s, t, f) {
            if (!s || !t) {
                return 0
            }
            var l = s.length > t.length ? s.length : t.length
            var n = s.length
            var m = t.length
            var d = []
            f = f || 3
            var min = function(a, b, c) {
                return a < b ? (a < c ? a : c) : (b < c ? b : c)
            }
            var i, j, si, tj, cost
            if (n === 0) return m
            if (m === 0) return n
            for (i = 0; i <= n; i++) {
                d[i] = []
                d[i][0] = i
            }
            for (j = 0; j <= m; j++) {
                d[0][j] = j
            }
            for (i = 1; i <= n; i++) {
                si = s.charAt(i - 1)
                for (j = 1; j <= m; j++) {
                    tj = t.charAt(j - 1)
                    if (si === tj) {
                        cost = 0
                    } else {
                        cost = 1
                    }
                    d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost)
                }
            }
            let res = (1 - d[n][m] / l)
            return res.toFixed(f)
        }
        function text_streamline(text){
            var too_much = ["?", "、", "？", "。", "，", ",", "的", "其實", "然後", "所謂的", "基本上", "你好", "您好",
                            "大概", "有", "很", "嗎", "老實說", "我這邊", "或", "呢", "之類的", "最近"];
            for(var i=0; i<too_much.length; i++){
                text = text.replace(too_much[i], "");
            }
            return text;
        }
    </script>
</body>

</html>