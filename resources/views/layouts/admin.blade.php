@inject('templateService', 'App\Services\AdminTemplateService')
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="../img/favicon.ico" type="image/x-icon" />
  <title>Dr. OSCE.ai{{ empty($title) ? '' : " | {$title}" }}</title>

  <!-- Pace -->
  <script src="{{ asset('plugins/pace-progress/pace.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/green/pace-theme-minimal.css') }}">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('plugins/adminlte/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->
  <!-- Customize Style -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/template.css') }}">
  @yield('header')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-info navbar-dark border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item"> 
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" onclick="logoutForm.submit()"><i class="fas fa-sign-out-alt"></i></a>
        <form name="logoutForm" action="{{ route('logout') }}" method="POST" class="hide">@csrf</form>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-info elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link text-center">
       Dr. OSCE<span class="brand-text font-weight-light">.ai</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          @foreach ($templateService->getSidebarMenu() as $menu)
          <li class="nav-item{{ is_array($menu['children']) ? ' as-treeview' : '' }}{{ $menu['is_active'] ? ' menu-open' : '' }}">
            <a href="{{ $menu['url'] }}" class="nav-link{{ $menu['is_active'] ? ' active' : '' }}">
              <i class="nav-icon {{ $menu['icon'] }}"></i>
              <p>
                {{ $menu['name'] }}
                @if (is_array($menu['children']) && count($menu['children']) > 0)
                <i class="right fas fa-angle-left"></i>
                @endif
              </p>
            </a>
            @if(is_array($menu['children']))
            <ul class="nav nav-treeview">
              @foreach ($menu['children'] as $child)
              <li class="nav-item">
                <a href="{{ $child['url'] }}" class="nav-link{{ $child['is_active'] ? ' active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ $child['name'] }}</p>
                </a>
              </li>
              @endforeach
            </ul>
            @endif
          </li>
          @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @isset($pageTitle)
              <h1 class="m-0 text-dark">{{ $pageTitle }}</h1>
            @endisset
          </div><!-- /.col -->
          <div class="col-sm-6">
            @yield('breadcrumb')
            <!--
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol>
            -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          @yield('content')
            
          @unless (empty($successMessage))
          <span class="d-none toast-message" data-type="success">{{ $successMessage }}</span>
          @endunless
          @unless (empty($errorMessage))
          <span class="d-none toast-message" data-type="error">{{ $errorMessage }}</span>
          @endunless
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
      <div class="text-sm text-center">
        <strong>Copyright &copy; {{ date('Y') === '2019' ? '2019' : date('2019-Y') }} <a href="">Dr. OSCE.ai</a>.</strong> All rights reserved.
      </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('plugins/adminlte/js/adminlte.min.js') }}"></script>
<!-- Customize Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script  src = "https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js" > </script>
<script>
</script>
@yield('scripts')
</body>
</html>
