<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dr.OSCE</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/loading.css" />
    <audio id="btnAudio">
        <source src="media/主要_按紐(調).mp3" type="audio/mpeg">
        未取得按紐音效
    </audio>
</head>

<body>
    <div id="dive-app" class="container">
        <div class="loading" title="點擊後(全螢幕)並開始"></div>
        <iframe src="http://dive.nutn.edu.tw:8080/Experiment//kaleTestExperiment5.jsp?eid=13745&record=false" name="dive13745" 
            class="dive" frameborder="0"></iframe>
        
        <div class="float-div">
            <button class="float-button" onclick="fullScreen()">全螢幕</button>
        </div>
    </div>
	<script src="http://dive.nutn.edu.tw:8080/Experiment/js/dive.linker.min.js"></script>
    <script src="js/linker.js"></script>
    <script>
        var user_name = 'test';
        var ask_text, ask_stat, ask_sim;
        var testPT = { //流程樹
            "0": {
                course: "0",
                question: "哪裡不舒服？",
                answer: "脖子有一顆東西",
                type: 1, //1:標題，2:內容
            },
            "1": {
                course: "1",
                question: "腫塊在哪裡?",
                answer: "頸部右側邊上半部(舉起右手，放在右臉頰下方脖子處)",
                type: 1, //1:標題，2:內容
            },
            "2": {
                course: "2",
                question: "怎麼發現的?",
                answer: "按摩時摸到的",
                type: 1, //1:標題，2:內容
            },
            "3": {
                course: "3",
                question: "大概出現多久了?",
                answer: "大約差不多2~3個月了",
                type: 1, //1:標題，2:內容
            }
        }
        function accin(){
            var input = drOSCE.diveLinker.getInputList();
            input = Object.values(input);
            for(let i=0; i<input.length; i++){
                if(input[i].name == "user_name"){
                    drOSCE.diveLinker.setInput(input[i].id, user_name);
                }
            }
        }
        function checkleave(){
            checkleavetf = setInterval(function(){
                var complete = drOSCE.diveLinker.getOutputList();
                complete = Object.values(complete);
                for(let i=0; i<complete.length; i++){
                    if(complete[i].name == "leave" && complete[i].value == 1){
                        window.location.href = "student";
                        clearInterval(checkleavetf);
                    }
                }
            }, 100);
        }
        function checkfull(){
            checkfull = setInterval(function(){
                if( window.innerHeight == screen.height) {
                    $( ".float-button" ).css( "display", "none" );
                }else{
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                        $( ".float-button" ).css( "display", "inline-block" );
                    }else{
                        $( ".float-button" ).css( "display", "inline-block" );
                        $( ".float-button" ).css( "padding", "5px 10px" );
                    }
                }
            }, 100);
        }
        function askinit(){
            var ask = drOSCE.diveLinker.getOutputList();
            ask = Object.values(ask);
            var PTid = [];
            for(let i=0; i<ask.length; i++){
                if(ask[i].name == "文字"){
                    ask_text = ask[i].id;
                }
                if(ask[i].name == "狀態"){
                    ask_stat = ask[i].id;
                }
                if(ask[i].name == "相似度"){
                    ask_sim = ask[i].id;
                }
            }
            checkstattf = setInterval(function(){
                if(drOSCE.diveLinker.getAttr(ask_stat) == 1){
                    PTid = [];
                    drOSCE.diveLinker.setInput(ask_stat, 2);
                    var text = drOSCE.diveLinker.getAttr(ask_text);
                    for(var PT in testPT){
                        var sim = similar(text, testPT[PT].question);
                        if(sim > 0.3){
                            console.log("yes" + testPT[PT].course + sim);
                            drOSCE.diveLinker.setInput(ask_sim, sim);
                            PTid.push(testPT[PT].course);
                        }
                        else{
                            console.log("no" + testPT[PT].course + sim);
                        }
                    }
                    drOSCE.diveLinker.setInput(ask_stat, 0);
                    console.log(PTid);
                }
            }, 100);
        }
        function similar(s, t, f) {
            if (!s || !t) {
                return 0
            }
            var l = s.length > t.length ? s.length : t.length
            var n = s.length
            var m = t.length
            var d = []
            f = f || 3
            var min = function(a, b, c) {
                return a < b ? (a < c ? a : c) : (b < c ? b : c)
            }
            var i, j, si, tj, cost
            if (n === 0) return m
            if (m === 0) return n
            for (i = 0; i <= n; i++) {
                d[i] = []
                d[i][0] = i
            }
            for (j = 0; j <= m; j++) {
                d[0][j] = j
            }
            for (i = 1; i <= n; i++) {
                si = s.charAt(i - 1)
                for (j = 1; j <= m; j++) {
                    tj = t.charAt(j - 1)
                    if (si === tj) {
                        cost = 0
                    } else {
                        cost = 1
                    }
                    d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost)
                }
            }
            let res = (1 - d[n][m] / l)
            return res.toFixed(f)
        }
    </script>
</body>

</html>