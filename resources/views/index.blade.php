<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dr.OSCE</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/loading.css" />
</head>

<body>
    <div id="dive-app" class="container">
        <div class="loading" title="點擊後(全螢幕)並開始"></div>
		<iframe src="https://dive.nutn.edu.tw/Experiment//kaleTestExperiment5.jsp?eid=10469&record=false" name="dive10469" 
			class="dive" frameborder="0"></iframe>
    </div>
	<script src="https://dive.nutn.edu.tw/Experiment/js/dive.linker.min.js"></script>
    <script src="js/linker.js"></script>
    <script src="js/moment.js"></script>
    <script>
        var name = '{{ $user->name}}';
        var score;

        function accin(){
            $.ajax({
                url: 'http://120.114.170.26/Dr.OSCE/public/home/getscore',
                method: 'GET',
                data: {'_token': "{{ csrf_token() }}"},
                error: function(result) {
                    console.log('fail');
                },
                success: function(result) {
                    score = result;
                    console.log('success');
                    var input = drOSCE.diveLinker.getInputList();
                    input = Object.values(input);
                    for(let i=0; i<input.length; i++){
                        if(input[i].name == "使用者"){
                            var namein = input[i].id;
                            drOSCE.diveLinker.setInput(namein, name);
                        }
                        if(input[i].name == "home_havescore"){
                            var havescore = input[i].id;
                            drOSCE.diveLinker.setInput(havescore, Object.keys(score).length);
                        }
                        if(input[i].name == "home_time1"){
                            var time1 = input[i].id;
                        }
                        if(input[i].name == "home_time2"){
                            var time2 = input[i].id;
                        }
                        if(input[i].name == "home_time3"){
                            var time3 = input[i].id;
                        }
                    }
                    if(Object.keys(score).length != 0){
                        sctimein(time1, time2, time3); 
                    }
                }
                });
        }
        function sctimein(time1, time2, time3){
            var now = -1;
            var nowindex = drOSCE.diveLinker.getOutputList();
            nowindex = Object.values(nowindex);
            for(let i=0; i<nowindex.length; i++){
                if(nowindex[i].name == "home_nowindex"){
                    var nowindex = nowindex[i].id;
                }
            }
            sctime = setInterval(function(){
                var nowin = drOSCE.diveLinker.getAttr(nowindex);
                nowin = parseInt(nowin);
                if(now != nowin){
                    drOSCE.diveLinker.setInput(time1, moment(score[nowin].created_at).format('YYYY-MM-DD, h:mm:ss a'));
                    if(score.length > (nowin+1)){
                        drOSCE.diveLinker.setInput(time2, moment(score[nowin+1].created_at).format('YYYY-MM-DD, h:mm:ss a'));
                    }
                    else if(score.length <= (nowin+1)){
                        drOSCE.diveLinker.setInput(time2, 0);
                    }
                    if(score.length > (nowin+2)){
                        drOSCE.diveLinker.setInput(time3, moment(score[nowin+2].created_at).format('YYYY-MM-DD, h:mm:ss a'));
                    }
                    else if(score.length <= (nowin+2)){
                        drOSCE.diveLinker.setInput(time3, 0);
                    }
                    now = nowin;
                }
            }, 100);
        }
        function getdata(){
            var getda = drOSCE.diveLinker.getOutputList();
            getda = Object.values(getda);
            for(let i=0; i<getda.length; i++){
                if(getda[i].name == "home_whscore"){
                    getda = getda[i].id;
                    getda = attrbug(getda);
                    getda = parseInt(getda);
                    return score[getda];
                }
            }
        }
        function attrbug(getda){
            getda = drOSCE.diveLinker.getAttr(getda);
            return getda;
        }
        function savedata(tt){
            $.ajax({
                url: 'http://120.114.170.26/Dr.OSCE/public/home/save',
                method: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    score: tt.score.v,
                    tmt: tt.tmt.v,
                    tmc: tt.tmc.v,
                    tmh: tt.tmh.v,
                    tmm: tt.tmm.v,
                    sct: tt.sct.v,
                    scc: tt.scc.v,
                    sch: tt.sch.v,
                    scm: tt.scm.v,
                    answeruser: tt.answeruser.v,
                    answercase: tt.answercase.v,
                    his22: tt.his22.v,
                    his41: tt.his41.v,
                    meduser: tt.meduser.v,
                    bmeduser: tt.bmeduser.v,
                    medanswer: tt.medanswer.v,
                    his11: tt.his11.v,
                    his12: tt.his12.v,
                    his13: tt.his13.v,
                    his21: tt.his21.v,
                    his31: tt.his31.v,
                    his32: tt.his32.v,
                    his42: tt.his42.v,
                    his43: tt.his43.v,
                    his51: tt.his51.v,
                    his52: tt.his52.v,
                    his53: tt.his53.v,
                    his61: tt.his61.v,
                    criticalf: tt.criticalf.v,
                    criticals: tt.criticals.v,
                    roundf: tt.roundf.v,
                    questionf: tt.questionf.v,
                    rounds: tt.rounds.v,
                    questions: tt.questions.v,
                    tmhf: tt.tmhf.v,
                    tmhs: tt.tmhs.v,
                    tmht: tt.tmht.v,
                    blood: tt.blood.v,
                    dissc1: tt.dissc1.v,
                    dissc2: tt.dissc2.v,
                    dissc3: tt.dissc3.v,
                    dissc4: tt.dissc4.v,
                    dissc5: tt.dissc5.v,
                    dissc6: tt.dissc6.v
                },
                error: function(response) {
                    console.log('fail');
                },
                success: function(response) {
                    console.log('success');
                }
                });
        }
        function testsave(){
            var tt = new Object();
            tt.score = 50;
            tt.tmt = 1;
            tt.tmc = 1;
            tt.tmh = 1;
            tt.tmm = 1;
            tt.sct = 1;
            tt.scc = 1;
            tt.sch = 1;
            tt.scm = 1;
            tt.answeruser = 1;
            tt.answercase = 1;
            tt.his22 = 1;
            tt.his41 = 1;
            tt.meduser = 1;
            tt.bmeduser = 1;
            tt.medanswer = 1;
            tt.his11 = 1;
            tt.his12 = 1;
            tt.his13 = 1;
            tt.his21 = 1;
            tt.his31 = 1;
            tt.his32 = 1;
            tt.his42 = 1;
            tt.his43 = 1;
            tt.his51 = 1;
            tt.his52 = 1;
            tt.his53 = 1;
            tt.his61 = 1;
            tt.criticalf = 1;
            tt.criticals = 1;
            tt.roundf = 1;
            tt.questionf = 1;
            tt.rounds = 1;
            tt.questions = 1;
            tt.tmhf = 1;
            tt.tmhs = 1;
            tt.tmht = 1;
            tt.blood = 1;
            tt.dissc1 = 1;
            tt.dissc2 = 1;
            tt.dissc3 = 1;
            tt.dissc4 = 1;
            tt.dissc5 = 1;
            tt.dissc6 = 1;
            $.ajax({
                url: 'http://120.114.170.26/Dr.OSCE/public/home/save',
                method: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    score: tt.score,
                    tmt: tt.tmt,
                    tmc: tt.tmc,
                    tmh: tt.tmh,
                    tmm: tt.tmm,
                    sct: tt.sct,
                    scc: tt.scc,
                    sch: tt.sch,
                    scm: tt.scm,
                    answeruser: tt.answeruser,
                    answercase: tt.answercase,
                    his22: tt.his22,
                    his41: tt.his41,
                    meduser: tt.meduser,
                    bmeduser: tt.bmeduser,
                    medanswer: tt.medanswer,
                    his11: tt.his11,
                    his12: tt.his12,
                    his13: tt.his13,
                    his21: tt.his21,
                    his31: tt.his31,
                    his32: tt.his32,
                    his42: tt.his42,
                    his43: tt.his43,
                    his51: tt.his51,
                    his52: tt.his52,
                    his53: tt.his53,
                    his61: tt.his61,
                    criticalf: tt.criticalf,
                    criticals: tt.criticals,
                    roundf: tt.roundf,
                    questionf: tt.questionf,
                    rounds: tt.rounds,
                    questions: tt.questions,
                    tmhf: tt.tmhf,
                    tmhs: tt.tmhs,
                    tmht: tt.tmht,
                    blood: tt.blood,
                    dissc1: tt.dissc1,
                    dissc2: tt.dissc2,
                    dissc3: tt.dissc3,
                    dissc4: tt.dissc4,
                    dissc5: tt.dissc5,
                    dissc6: tt.dissc6,

                },
                error: function(response) {
                    console.log('fail');
                },
                success: function(response) {
                    console.log('success');
                }
                });
        }
    </script>
</body>

</html>